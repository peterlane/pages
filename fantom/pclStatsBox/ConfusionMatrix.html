<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
 "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns='http://www.w3.org/1999/xhtml'>
<head>
<title>pclStatsBox::ConfusionMatrix</title>
<meta http-equiv='Content-Type' content='text/html; charset=UTF-8'/>
<link rel='stylesheet' type='text/css' href='../style.css' />
</head>
<body>
<div class='breadcrumb'>
<ul>
<li><a href='../index.html'>Doc Index</a></li><li><a href='index.html'>pclStatsBox</a></li><li><a href='ConfusionMatrix.html'>ConfusionMatrix</a></li></ul>
</div>
<div class='mainSidebar'>
<div class='main type'>
<h1><span>class</span> pclStatsBox::ConfusionMatrix</h1>
<pre><a href='https://fantom.org/doc/sys/Obj.html'>sys::Obj</a>
  pclStatsBox::ConfusionMatrix</pre>

<p>A Confusion Matrix is often used in statistics or machine learning to hold the number of observed against predicted labels from an experiment.</p>

<p>A confusion matrix represents "the relative frequencies with which each of a number of stimuli is mistaken for each of the others by a person in a task requiring recognition or identification of stimuli" (R. Colman, A Dictionary of Psychology, 2008). Each row represents the predicted label of an instance, and each column represents the observed label of that instance. Numbers at each (row, column) reflect the total number of instances of predicted label "row" which were observed as having label "column".</p>

<p>A two-class example is:</p>

<pre>Observed        Observed      | 
Positive        Negative      | Predicted
------------------------------+------------
    a               b         | Positive
    c               d         | Negative</pre>

<p>Here the value:</p>

<ul>
<li><code>a</code> the true positives (those predicted positive and observed positive)</li>

<li><code>b</code> the false negatives (those predicted positive but observed negative)</li>

<li><code>c</code> the false positives (those predicted negative but observed positive)</li>

<li><code>d</code> the true negatives (those predicted negative and observed negative)</li>
</ul>

<p>From this table we can calculate statistics like:</p>

<ul>
<li>true positive rate - a/(a+b)</li>

<li>positive recall - a/(a+c)</li>
</ul>

<p>As statistics can also be calculated for the negative label, e.g. the true negative rate is d/(c+d), the functions below have an optional "label" parameter, to specify which label they are calculated for: the default is to report for the first label named when the matrix is created</p>

<p>The implementation supports confusion matrices with more than two labels. When more than two labels are in use, the statistics are calculated as if the first, or named, label were positive and all the other labels are grouped as if negative.</p>

<h1>Usage</h1>

<p>The following example creates a simple two-label confusion matrix, prints a few statistics and displays the table:</p>

<pre>using pclStatsBox

class ExampleConfusionMatrix
{
  static Void main()
  {
    cm := ConfusionMatrix(["pos", "neg"])

    cm.addCount("pos", "pos", 10)
    cm.addCount("pos", "neg", 3)
    cm.addCount("neg", "neg", 20)
    cm.addCount("neg", "pos", 5)

    echo("Confusion Matrix")
    echo("")
    echo(cm)
    echo("Precision: ${cm.precision}")
    echo("Recall   : ${cm.recall}")
    echo("MCC      : ${cm.matthewsCorrelation}")
  }
}</pre>

<p>which outputs:</p>

<pre>Confusion Matrix

Observed|
pos neg | Predicted
--------+----------
 10   3 | pos
  5  20 | neg

Precision: 0.6666666666666666
Recall   : 0.7692307692307693
MCC      : 0.5524850114241865</pre>
<dl>
<dt id='addCount'>
addCount</dt>
<dd>
<p class='sig'>
<code> <a href='https://fantom.org/doc/sys/Void.html'>Void</a> addCount(<a href='https://fantom.org/doc/sys/Str.html'>Str</a> predicted, <a href='https://fantom.org/doc/sys/Str.html'>Str</a> observed, <a href='https://fantom.org/doc/sys/Int.html'>Int</a> count := 1)</code></p>

<p>Adds total to the count for given (predicted, observed) labels. Throws an error if labels are not valid.</p>
</dd>
<dt id='cohenKappa'>
cohenKappa</dt>
<dd>
<p class='sig'>
<code> <a href='https://fantom.org/doc/sys/Float.html'>Float</a> cohenKappa(<a href='https://fantom.org/doc/sys/Str.html'>Str</a> label := this.labels.first())</code></p>

<p>Cohen's Kappa statistic compares observed accuracy with an expected accuracy.</p>
</dd>
<dt id='count'>
count</dt>
<dd>
<p class='sig'>
<code> <a href='https://fantom.org/doc/sys/Int.html'>Int</a> count(<a href='https://fantom.org/doc/sys/Str.html'>Str</a> predicted, <a href='https://fantom.org/doc/sys/Str.html'>Str</a> observed)</code></p>

<p>Returns count for given (predicted, observed) labels. Throws an error if labels are not valid.</p>
</dd>
<dt id='fMeasure'>
fMeasure</dt>
<dd>
<p class='sig'>
<code> <a href='https://fantom.org/doc/sys/Float.html'>Float</a> fMeasure(<a href='https://fantom.org/doc/sys/Str.html'>Str</a> label := this.labels.first())</code></p>

<p>Harmonic mean of the precision and recall for the given label.</p>
</dd>
<dt id='falseNegative'>
falseNegative</dt>
<dd>
<p class='sig'>
<code> <a href='https://fantom.org/doc/sys/Int.html'>Int</a> falseNegative(<a href='https://fantom.org/doc/sys/Str.html'>Str</a> label := this.labels.first())</code></p>

<p>Returns the number of instances of the given label which are incorrectly observed.</p>
</dd>
<dt id='falsePositive'>
falsePositive</dt>
<dd>
<p class='sig'>
<code> <a href='https://fantom.org/doc/sys/Int.html'>Int</a> falsePositive(<a href='https://fantom.org/doc/sys/Str.html'>Str</a> label := this.labels.first())</code></p>

<p>Returns the number of instances incorrectly observed as the given label.</p>
</dd>
<dt id='falseRate'>
falseRate</dt>
<dd>
<p class='sig'>
<code> <a href='https://fantom.org/doc/sys/Float.html'>Float</a> falseRate(<a href='https://fantom.org/doc/sys/Str.html'>Str</a> label := this.labels.first())</code></p>

<p>Returns the proportion of instances of given label incorrectly observed out of all instances not originally of that label.</p>
</dd>
<dt id='geometricMean'>
geometricMean</dt>
<dd>
<p class='sig'>
<code> <a href='https://fantom.org/doc/sys/Float.html'>Float</a> geometricMean()</code></p>

<p>Nth root of product of true-rate for each label</p>
</dd>
<dt id='make'>
make</dt>
<dd>
<p class='sig'>
<code>new make(<a href='https://fantom.org/doc/sys/Str.html'>Str</a>[] labels := [&quot;positive&quot;,&quot;negative&quot;])</code></p>

<p>Constructor takes a list of labels for the confusion matrix. There should be at least two labels, and the default is ("positive", "negative")</p>
</dd>
<dt id='matthewsCorrelation'>
matthewsCorrelation</dt>
<dd>
<p class='sig'>
<code> <a href='https://fantom.org/doc/sys/Float.html'>Float</a> matthewsCorrelation(<a href='https://fantom.org/doc/sys/Str.html'>Str</a> label := this.labels.first())</code></p>

<p>Matthew's Correlation is a measure of the quality of binary classification.</p>
</dd>
<dt id='overallAccuracy'>
overallAccuracy</dt>
<dd>
<p class='sig'>
<code> <a href='https://fantom.org/doc/sys/Float.html'>Float</a> overallAccuracy()</code></p>

<p>Proportion of instances which are correctly observed.</p>
</dd>
<dt id='precision'>
precision</dt>
<dd>
<p class='sig'>
<code> <a href='https://fantom.org/doc/sys/Float.html'>Float</a> precision(<a href='https://fantom.org/doc/sys/Str.html'>Str</a> label := this.labels.first())</code></p>

<p>Precision is the proportion of instances of given label which are correctly observed.</p>
</dd>
<dt id='prevalence'>
prevalence</dt>
<dd>
<p class='sig'>
<code> <a href='https://fantom.org/doc/sys/Float.html'>Float</a> prevalence(<a href='https://fantom.org/doc/sys/Str.html'>Str</a> label := this.labels.first())</code></p>

<p>Prevalence is proportion of instances of given label out of total.</p>
</dd>
<dt id='recall'>
recall</dt>
<dd>
<p class='sig'>
<code> <a href='https://fantom.org/doc/sys/Float.html'>Float</a> recall(<a href='https://fantom.org/doc/sys/Str.html'>Str</a> label := this.labels.first())</code></p>

<p>Recall is equal to the trueRate, for a given label.</p>
</dd>
<dt id='sensitivity'>
sensitivity</dt>
<dd>
<p class='sig'>
<code> <a href='https://fantom.org/doc/sys/Float.html'>Float</a> sensitivity(<a href='https://fantom.org/doc/sys/Str.html'>Str</a> label := this.labels.first())</code></p>

<p>Sensitivity is another name for the true positive rate (recall).</p>
</dd>
<dt id='specificity'>
specificity</dt>
<dd>
<p class='sig'>
<code> <a href='https://fantom.org/doc/sys/Float.html'>Float</a> specificity(<a href='https://fantom.org/doc/sys/Str.html'>Str</a> label := this.labels.first())</code></p>

<p>Specificity is 1-falseRate for a given label.</p>
</dd>
<dt id='toStr'>
toStr</dt>
<dd>
<p class='sig'>
<code>virtual override <a href='https://fantom.org/doc/sys/Str.html'>Str</a> toStr()</code></p>

<p>Returns a string representation of the matrix across multiple lines in a table-like format.</p>
</dd>
<dt id='total'>
total</dt>
<dd>
<p class='sig'>
<code> <a href='https://fantom.org/doc/sys/Int.html'>Int</a> total()</code></p>

<p>Returns total of all counts.</p>
</dd>
<dt id='trueNegative'>
trueNegative</dt>
<dd>
<p class='sig'>
<code> <a href='https://fantom.org/doc/sys/Int.html'>Int</a> trueNegative(<a href='https://fantom.org/doc/sys/Str.html'>Str</a> label := this.labels.first())</code></p>

<p>Returns the number of instances NOT of the given label which are correctly observed.</p>
</dd>
<dt id='truePositive'>
truePositive</dt>
<dd>
<p class='sig'>
<code> <a href='https://fantom.org/doc/sys/Int.html'>Int</a> truePositive(<a href='https://fantom.org/doc/sys/Str.html'>Str</a> label := this.labels.first())</code></p>

<p>Returns the number of instances of the given label correctly observed.</p>
</dd>
<dt id='trueRate'>
trueRate</dt>
<dd>
<p class='sig'>
<code> <a href='https://fantom.org/doc/sys/Float.html'>Float</a> trueRate(<a href='https://fantom.org/doc/sys/Str.html'>Str</a> label := this.labels.first())</code></p>

<p>Returns the proportion of instances of the given label which are correctly observed.</p>
</dd>
</dl>
</div>
<div class='sidebar'>
<h3>Source</h3>
<ul>
<li>Not available</li></ul>
<h3>Slots</h3>
<ul>
<li><a href='#addCount'>addCount</a></li><li><a href='#cohenKappa'>cohenKappa</a></li><li><a href='#count'>count</a></li><li><a href='#fMeasure'>fMeasure</a></li><li><a href='#falseNegative'>falseNegative</a></li><li><a href='#falsePositive'>falsePositive</a></li><li><a href='#falseRate'>falseRate</a></li><li><a href='#geometricMean'>geometricMean</a></li><li><a href='#make'>make</a></li><li><a href='#matthewsCorrelation'>matthewsCorrelation</a></li><li><a href='#overallAccuracy'>overallAccuracy</a></li><li><a href='#precision'>precision</a></li><li><a href='#prevalence'>prevalence</a></li><li><a href='#recall'>recall</a></li><li><a href='#sensitivity'>sensitivity</a></li><li><a href='#specificity'>specificity</a></li><li><a href='#toStr'>toStr</a></li><li><a href='#total'>total</a></li><li><a href='#trueNegative'>trueNegative</a></li><li><a href='#truePositive'>truePositive</a></li><li><a href='#trueRate'>trueRate</a></li></ul>
</div>
</div>
</body>
</html>
