<!DOCTYPE html>
<html lang="en">
<head>
    <link rel="Stylesheet" type="text/css" href="../style.css" />
    <link rel="Stylesheet" type="text/css" href="../rouge-style.css" />
    <title>2024-10-25</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
</head>
<body>
    <div class="content">
    
<h1>2024-10-25: Text Classification with ABCL and Weka</h1>

<p>
This is an exercise I've done in a few JVM-based languages.
The idea is to do some simple text classification in Lisp 
using the <a href="http://www.cs.waikato.ac.nz/ml/weka/">Weka</a> library.
As we are working on the JVM, the code runs in ABCL.
</p>

<p>
For data, I use the BBC dataset from
<a href="https://www.kaggle.com/shivamkushwaha/bbc-full-text-document-classification">Kaggle</a>,
selecting two classes: entertainment and tech. 
</p>

<p>
So I have a parent folder, "bbc/", with two child folders, "entertainment" and
"tech". Each file in the child folders is a text document, and will form an
instance for the classifier to work on. The classes each have around 400
documents, and the total word count is 330,000.
</p>

<p>
To install Weka, <a href="https://waikato.github.io/weka-wiki/downloading_weka/">download</a>
the latest version (I used 3.8.6), and extract the file weka.jar.
</p>

<h2>Overall process</h2>

<p>
The overall process can be divided into several steps. (Here, I keep things
relatively simple, to investigate the API.)
</p>

<p>
First, the text must be converted into words only, so all numbers and
punctuation are removed, and all letters are converted to lowercase. So
"Address: 3 High Street, London." becomes the four words "address high street
london".
</p>

<p>
Second, it is useful to simplify the words themselves. Common words (like
"the", "a") are removed, using a stop-list. Also, stemming is applied so that
different words like "walks" and "walking" are reduced to their stem "walk";
stemming increases the number of words in different documents which match.
</p>

<p>
Third, the text files are converted into a representation where each text file
is an <em>instance</em> made from the <em>values</em> for a number of <em>attributes</em>, and a
<em>class</em> label. The class label has two values, one for "entertainment" and one
for "tech", depending on which folder the text file is in. Each attribute
represents one word, and has values 1 or 0 (whether the word for that attribute
is present in this instance or not).
</p>

<p>
Fourth, the number of attributes is usually very large at this stage, because
natural language uses a lot of words. An attribute selection technique reduces
the number of attributes to a manageable size.
</p>

<p>
Fifth, build some classification models. Three algorithms are used: Naive
Bayes, Decision Trees and Support Vector Machines. For each of these
algorithms, 10-fold cross validation is used to derive an overall accuracy.
</p>

<h2>Script</h2>

<p>
The script follows. It is actually one file, but broken up with some
explanations. 
</p>

<p>
<a href="https://bitbucket.org/pclprojects/workspace/snippets/rqG4Md/abcl-text-classification-with-weka">https://bitbucket.org/pclprojects/workspace/snippets/rqG4Md/abcl-text-classification-with-weka</a>
</p>

<p>
At the start of the script, import the required libraries. 
</p>

<pre type=lisp>
<span class="p">(</span><span class="nb">require</span> <span class="ss">:asdf</span><span class="p">)</span>
<span class="p">(</span><span class="nb">require</span> <span class="ss">:confusion-matrix</span><span class="p">)</span> <span class="c1">; &lt;1&gt;</span>
<span class="p">(</span><span class="nb">require</span> <span class="ss">:java</span><span class="p">)</span>
</pre>
<ol>
<li>
Install <a href="https://peterlane.codeberg.page/confusion-matrix-lisp/">Confusion Matrix</a> - my own library providing a Confusion Matrix.

</ol>
<p>
The following method was created as our preprocess steps apply two of Weka's
filters. Weka requires us to input each instance in turn to the filter, tell
the filter the current batch has finished, and then retrieve the instances.
Notice that the output <code>result</code> may have a different structure (number and type
of attributes) to the input <code>instances</code>. 
</p>

<p>
This method illustrates one of ABCL's JFFI methods: <code>jcall</code>, which takes the 
name of the method, the object to use and the arguments. e.g. the expression
</p>

<pre type=lisp>
  <span class="p">(</span><span class="nv">jcall</span> <span class="s">"setInputFormat"</span> <span class="nv">filterfn</span> <span class="nv">instances</span><span class="p">)</span>
</pre>

<p>
is equivalent to the Java call:
</p>

<pre type=java>
  <span class="n">filterfn</span><span class="o">.</span><span class="na">setInputFormat</span> <span class="o">(</span><span class="n">instances</span><span class="o">);</span>
</pre>

<pre type=lisp>
<span class="p">(</span><span class="nb">defun</span> <span class="nv">apply-filter</span> <span class="p">(</span><span class="nv">instances</span> <span class="nv">filter-fn</span><span class="p">)</span>
  <span class="p">(</span><span class="nv">jcall</span> <span class="s">"setInputFormat"</span> <span class="nv">filter-fn</span> <span class="nv">instances</span><span class="p">)</span>
  <span class="p">(</span><span class="nb">dotimes</span> <span class="p">(</span><span class="nv">i</span> <span class="p">(</span><span class="nv">jcall</span> <span class="s">"size"</span> <span class="nv">instances</span><span class="p">))</span>                   <span class="c1">; &lt;1&gt;</span>
    <span class="p">(</span><span class="nv">jcall</span> <span class="s">"input"</span> <span class="nv">filter-fn</span> <span class="p">(</span><span class="nv">jcall</span> <span class="s">"get"</span> <span class="nv">instances</span> <span class="nv">i</span><span class="p">)))</span>  <span class="c1">; &lt;2&gt;</span>
  <span class="p">(</span><span class="nv">jcall</span> <span class="s">"batchFinished"</span> <span class="nv">filter-fn</span><span class="p">)</span>

  <span class="p">(</span><span class="k">let</span> <span class="p">((</span><span class="nv">result</span> <span class="p">(</span><span class="nv">jcall</span> <span class="s">"getOutputFormat"</span> <span class="nv">filter-fn</span><span class="p">)))</span>
    <span class="p">(</span><span class="nb">do</span> <span class="p">((</span><span class="nv">instance</span> <span class="p">(</span><span class="nv">jcall</span> <span class="s">"output"</span> <span class="nv">filter-fn</span><span class="p">)</span>
                   <span class="p">(</span><span class="nv">jcall</span> <span class="s">"output"</span> <span class="nv">filter-fn</span><span class="p">)))</span>
      <span class="p">((</span><span class="nb">not</span> <span class="nv">instance</span><span class="p">)</span> <span class="nv">result</span><span class="p">)</span>
      <span class="p">(</span><span class="nv">jcall</span> <span class="s">"add"</span> <span class="nv">result</span> <span class="nv">instance</span><span class="p">))))</span>
</pre>
<ol>
<li>
Use a loop based on index to iterate over the Java Instances.

<li>
The <code>Instance#get</code> method returns an instance..

</ol>
<p>
The preprocess method covers the first four(!) steps described above. 
</p>

<p>
Weka provides <code>TextDirectoryLoader</code> to load the text documents from the two
folders. This process leaves each instance with two attributes: one is the text
of the document, and the second is its class label (the name of the child
folder). 
</p>

<p>
Step 1 is done using Lisp's <code>substitute-if-not</code>, to replace all non-alphabetic
characters with spaces. 
</p>

<p>
Steps 2-3 are done using a <code>StringToWordVector</code> filter. In this filter, I set
the stemmer and stopwords handlers, tell it to convert the text to lower case
and tokenise the string as words (rather than character sequences). Setting
<code>output_word_counts</code> to false means the values will be 1 or 0, not actual word
counts.
</p>

<p>
Step 4 is achieved using a second filter, <code>CorrelationAttributeEval</code>, along
with a ranking algorithm to pick the most predictive 300 attributes.
</p>

<p>
Here, we use <code>jnew</code> to instantiate Java classes.
</p>

<pre type=lisp>
<span class="p">(</span><span class="nb">defun</span> <span class="nv">pre-process</span> <span class="p">(</span><span class="nv">text-directory</span><span class="p">)</span>
  <span class="p">(</span><span class="k">let</span> <span class="p">((</span><span class="nv">loader</span> <span class="p">(</span><span class="nv">jnew</span> <span class="s">"weka.core.converters.TextDirectoryLoader"</span><span class="p">)))</span>
    <span class="p">(</span><span class="nv">jcall</span> <span class="s">"setSource"</span> <span class="nv">loader</span> <span class="p">(</span><span class="nv">jnew</span> <span class="s">"java.io.File"</span> <span class="nv">text-directory</span><span class="p">))</span>
    <span class="p">(</span><span class="k">let</span> <span class="p">((</span><span class="nv">instances</span> <span class="p">(</span><span class="nv">jcall</span> <span class="s">"getDataSet"</span> <span class="nv">loader</span><span class="p">)))</span>

      <span class="c1">; remove numbers/punctuation - step 1</span>
      <span class="p">(</span><span class="nb">dotimes</span> <span class="p">(</span><span class="nv">i</span> <span class="p">(</span><span class="nv">jcall</span> <span class="s">"size"</span> <span class="nv">instances</span><span class="p">))</span>
        <span class="p">(</span><span class="k">let</span> <span class="p">((</span><span class="nv">instance</span> <span class="p">(</span><span class="nv">jcall</span> <span class="s">"get"</span> <span class="nv">instances</span> <span class="nv">i</span><span class="p">)))</span>
          <span class="c1">; remove all non ASCII letters</span>
          <span class="p">(</span><span class="nv">jcall</span> <span class="s">"setValue"</span> <span class="nv">instance</span> <span class="mi">0</span> 
                   <span class="p">(</span><span class="nb">substitute-if-not</span> <span class="sc">#\space</span>                     <span class="c1">; &lt;1&gt;</span>
                                      <span class="nf">#'</span><span class="nb">alpha-char-p</span>
                                      <span class="p">(</span><span class="nv">jcall</span> <span class="s">"stringValue"</span> <span class="nv">instance</span> <span class="mi">0</span><span class="p">)))))</span>

      <span class="c1">; turn into vector of words, applying filters - steps 2 &amp; 3   &lt;2&gt;</span>
      <span class="p">(</span><span class="k">let</span> <span class="p">((</span><span class="nv">string-&gt;words</span> <span class="p">(</span><span class="nv">jnew</span> <span class="s">"weka.filters.unsupervised.attribute.StringToWordVector"</span><span class="p">))</span>
        <span class="p">(</span><span class="nv">jcall</span> <span class="s">"setLowerCaseTokens"</span> <span class="nv">string-&gt;words</span> <span class="no">t</span><span class="p">)</span>
        <span class="p">(</span><span class="nv">jcall</span> <span class="s">"setOutputWordCounts"</span> <span class="nv">string-&gt;words</span> <span class="no">nil</span><span class="p">)</span>
        <span class="p">(</span><span class="nv">jcall</span> <span class="s">"setStemmer"</span> <span class="nv">string-&gt;words</span> <span class="p">(</span><span class="nv">jnew</span> <span class="s">"weka.core.stemmers.LovinsStemmer"</span><span class="p">))</span>
        <span class="p">(</span><span class="nv">jcall</span> <span class="s">"setStopwordsHandler"</span> <span class="nv">string-&gt;words</span> <span class="p">(</span><span class="nv">jnew</span> <span class="s">"weka.core.stopwords.Rainbow"</span><span class="p">))</span>
        <span class="p">(</span><span class="nv">jcall</span> <span class="s">"setTokenizer"</span> <span class="nv">string-&gt;words</span> <span class="p">(</span><span class="nv">jnew</span> <span class="s">"weka.core.tokenizers.WordTokenizer"</span><span class="p">))</span>
        <span class="c1">; -- apply the filter</span>
        <span class="p">(</span><span class="nb">setf</span> <span class="nv">instances</span> <span class="p">(</span><span class="nv">apply-filter</span> <span class="nv">instances</span> <span class="nv">string-&gt;words</span><span class="p">)))</span>

      <span class="c1">; identify the class label</span>
      <span class="p">(</span><span class="nv">jcall</span> <span class="s">"setClassIndex"</span> <span class="nv">instances</span> <span class="mi">0</span><span class="p">)</span>

      <span class="c1">; reduce number of attributes to 300 - step 4</span>
      <span class="p">(</span><span class="k">let</span> <span class="p">((</span><span class="nv">selector</span> <span class="p">(</span><span class="nv">jnew</span> <span class="s">"weka.filters.supervised.attribute.AttributeSelection"</span><span class="p">))</span>
            <span class="p">(</span><span class="nv">ranker</span> <span class="p">(</span><span class="nv">jnew</span> <span class="s">"weka.attributeSelection.Ranker"</span><span class="p">)))</span>
        <span class="p">(</span><span class="nv">jcall</span> <span class="s">"setEvaluator"</span> <span class="nv">selector</span> <span class="p">(</span><span class="nv">jnew</span> <span class="s">"weka.attributeSelection.CorrelationAttributeEval"</span><span class="p">))</span>
        <span class="p">(</span><span class="nv">jcall</span> <span class="s">"setNumToSelect"</span> <span class="nv">ranker</span> <span class="mi">300</span><span class="p">)</span>
        <span class="p">(</span><span class="nv">jcall</span> <span class="s">"setSearch"</span> <span class="nv">selector</span> <span class="nv">ranker</span><span class="p">)</span>
        <span class="c1">; apply the filter</span>
        <span class="p">(</span><span class="nb">setf</span> <span class="nv">instances</span> <span class="p">(</span><span class="nv">apply-filter</span> <span class="nv">instances</span> <span class="nv">selector</span><span class="p">)))</span>

      <span class="c1">; randomise order of instances</span>
      <span class="p">(</span><span class="nv">jcall</span> <span class="s">"randomize"</span> <span class="nv">instances</span> <span class="p">(</span><span class="nv">jnew</span> <span class="s">"java.util.Random"</span><span class="p">))</span>

  <span class="nv">instances</span><span class="p">)))</span>
</pre>
<ol>
<li>
Use <code>substitute-if-not</code> to replace non-letters with space.

<li>
The <code>StringToWordVector</code> filter offers many options, some require 
   providing a new instance (like the stemmer) and others a value
   (like whether to use lower case tokens).

</ol>
<p>
Step 5 is the task of the <code>evaluate-classifier</code> method, used to test a given
classification algorithm. Weka provides methods on instances to access
train/test sets for k-fold cross-validation, so we use those to build and
evaluate a classifier for each fold. We use the fully qualified path to each
class as a reference. This method uses a <code>confusion-matrix</code> instance to collate
results, and Weka's cross-validation methods to handle the train/test splits.
</p>

<pre type=lisp>
<span class="p">(</span><span class="nb">defun</span> <span class="nv">number-&gt;class-label</span> <span class="p">(</span><span class="nv">n</span><span class="p">)</span>                                          <span class="c1">; &lt;1&gt;</span>
  <span class="s">"Convert real number into a class keyword."</span>
  <span class="p">(</span><span class="k">if</span> <span class="p">(</span><span class="nb">&gt;</span> <span class="nv">n</span> <span class="mf">0.5</span><span class="p">)</span> <span class="ss">:positive</span> <span class="ss">:negative</span><span class="p">))</span>

<span class="p">(</span><span class="nb">defun</span> <span class="nv">evaluate-classifier</span> <span class="p">(</span><span class="nv">name</span> <span class="nv">classifier</span> <span class="nv">instances</span> <span class="nv">k</span><span class="p">)</span>
  <span class="s">"Evaluate and report results of classifier based on k-fold CV."</span>
  <span class="p">(</span><span class="k">let</span> <span class="p">((</span><span class="nv">cm</span> <span class="p">(</span><span class="nv">cm:make-confusion-matrix</span> <span class="ss">:labels</span> <span class="o">'</span><span class="p">(</span><span class="ss">:positive</span> <span class="ss">:negative</span><span class="p">))))</span> <span class="c1">; &lt;2&gt;</span>
    <span class="p">(</span><span class="nb">dotimes</span> <span class="p">(</span><span class="nv">i</span> <span class="nv">k</span><span class="p">)</span>
      <span class="p">(</span><span class="k">let</span> <span class="p">((</span><span class="nv">model</span> <span class="p">(</span><span class="nv">jnew</span> <span class="nv">classifier</span><span class="p">))</span>                                   <span class="c1">; &lt;3&gt;</span>
            <span class="p">(</span><span class="nv">train</span> <span class="p">(</span><span class="nv">jcall</span> <span class="s">"trainCV"</span> <span class="nv">instances</span> <span class="nv">k</span> <span class="nv">i</span><span class="p">))</span>                     <span class="c1">; &lt;4&gt;</span>
            <span class="p">(</span><span class="nv">test</span> <span class="p">(</span><span class="nv">jcall</span> <span class="s">"testCV"</span> <span class="nv">instances</span> <span class="nv">k</span> <span class="nv">i</span><span class="p">)))</span>
        <span class="p">(</span><span class="nv">jcall</span> <span class="s">"buildClassifier"</span> <span class="nv">model</span> <span class="nv">train</span><span class="p">)</span>
        <span class="p">(</span><span class="nb">dotimes</span> <span class="p">(</span><span class="nv">i</span> <span class="p">(</span><span class="nv">jcall</span> <span class="s">"size"</span> <span class="nv">test</span><span class="p">))</span>                                <span class="c1">; &lt;5&gt;</span>
          <span class="p">(</span><span class="k">let</span> <span class="p">((</span><span class="nv">instance</span> <span class="p">(</span><span class="nv">jcall</span> <span class="s">"get"</span> <span class="nv">test</span> <span class="nv">i</span><span class="p">)))</span>
            <span class="p">(</span><span class="nv">cm:confusion-matrix-add</span> 
              <span class="nv">cm</span>
              <span class="p">(</span><span class="nv">number-&gt;class-label</span> 
                <span class="p">(</span><span class="nv">jcall</span> <span class="s">"classValue"</span> <span class="nv">instance</span><span class="p">))</span> <span class="c1">; predicted class</span>
              <span class="p">(</span><span class="nv">number-&gt;class-label</span> 
                <span class="p">(</span><span class="nv">jcall</span> <span class="s">"classifyInstance"</span> <span class="nv">model</span> <span class="nv">instance</span><span class="p">)))))))</span> <span class="c1">; observed class</span>
    <span class="c1">; report results</span>
    <span class="p">(</span><span class="nb">format</span> <span class="no">t</span> <span class="s">"Classifier: ~a~&amp;"</span> <span class="nv">name</span><span class="p">)</span>
    <span class="p">(</span><span class="nb">format</span> <span class="no">t</span> <span class="s">" -- Precision      ~,3f~&amp;"</span> <span class="p">(</span><span class="nv">cm:precision</span> <span class="nv">cm</span><span class="p">))</span>            <span class="c1">; &lt;6&gt;</span>
    <span class="p">(</span><span class="nb">format</span> <span class="no">t</span> <span class="s">" -- Recall         ~,3f~&amp;"</span> <span class="p">(</span><span class="nv">cm:recall</span> <span class="nv">cm</span><span class="p">))</span>
    <span class="p">(</span><span class="nb">format</span> <span class="no">t</span> <span class="s">" -- Geometric mean ~,3f~&amp;"</span> <span class="p">(</span><span class="nv">cm:geometric-mean</span> <span class="nv">cm</span><span class="p">))))</span>
</pre>
<ol>
<li>
Simple function to convert a numeric prediction into a class label.

<li>
Create a <code>confusion-matrix</code> to collate results.

<li>
Construct an instance of the classifier type.

<li>
Use Weka's <code>trainCV</code> and <code>testCV</code> to create train/test splits for each fold
   of the cross-validation process.

<li>
Run through every test instance in turn, recording its predicted and 
   observed class in the confusion matrix.

<li>
Pull out aggregate results from the confusion matrix.

</ol>
<p>
The top-level simply loads in the dataset, through the <code>pre-process</code> method,
and then evaluates each classifier in turn. Notice how the classifiers are 
identified by their full class name.
</p>

<pre type=lisp>
<span class="p">(</span><span class="k">let</span> <span class="p">((</span><span class="nv">data</span> <span class="p">(</span><span class="nv">pre-process</span> <span class="s">"bbc/"</span><span class="p">)))</span>
  <span class="p">(</span><span class="nb">dolist</span> <span class="p">(</span><span class="nv">classifier</span> 
            <span class="o">'</span><span class="p">((</span><span class="s">"Decision Tree"</span> <span class="s">"weka.classifiers.trees.J48"</span><span class="p">)</span>
              <span class="p">(</span><span class="s">"Naive Bayes"</span> <span class="s">"weka.classifiers.bayes.NaiveBayes"</span><span class="p">)</span>
              <span class="p">(</span><span class="s">"Support Vector Machine"</span> <span class="s">"weka.classifiers.functions.SMO"</span><span class="p">)))</span>
    <span class="p">(</span><span class="nv">evaluate-classifier</span> <span class="p">(</span><span class="nb">first</span> <span class="nv">classifier</span><span class="p">)</span>
                         <span class="p">(</span><span class="nb">second</span> <span class="nv">classifier</span><span class="p">)</span>
                         <span class="nv">data</span>
                         <span class="mi">10</span><span class="p">)))</span>
<span class="p">(</span><span class="nv">exit</span><span class="p">)</span>
</pre>

<p>
On my system, the script runs through in about 30 seconds. The output is:
</p>

<pre>
$ java --add-opens java.base/java.lang=ALL-UNNAMED -cp abcl.jar:weka.jar org.armedbear.lisp.Main --load abcl-weka-text.lisp
Armed Bear Common Lisp 1.9.2
Java 23 Oracle Corporation
OpenJDK 64-Bit Server VM
Low-level initialization completed in 0.388 seconds.
Startup completed in 2.186 seconds.
Classifier: Decision Tree
 -- Precision      0.950
 -- Recall         0.977
 -- Geometric mean 0.963
Classifier: Naive Bayes
 -- Precision      0.982
 -- Recall         0.974
 -- Geometric mean 0.978
Classifier: Support Vector Machine
 -- Precision      0.979
 -- Recall         0.990
 -- Geometric mean 0.985

</pre>

    </div>
    <hr>
    <small>Page from Peter's <a href="../index.html">Scrapbook</a>, output from a <a href="http://vimwiki.github.io/">VimWiki</a> on 2024-11-29.</small>
</body>
</html>
