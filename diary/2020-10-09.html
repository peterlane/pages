<html>
<head>
    <link rel="Stylesheet" type="text/css" href="../style.css" />
    <title>2020-10-09</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
</head>
<body>
    <div class="content">
    
<div id="2020-10-09: Repetition Codes"><h1 id="2020-10-09: Repetition Codes" class="header"><a href="#2020-10-09: Repetition Codes">2020-10-09: Repetition Codes</a></h1></div>

<p>
This is the first in what I plan to be a series of posts about <em>coding theory</em>,
looking at different techniques. 
</p>

<p>
I am working through two books:
</p>

<ul>
<li>
Raymond Hill, <em>A First Course in Coding Theory</em>, 1986.

<li>
Arkadii Slinko, <em>Algebra for Applications</em>, 2020.

</ul>
<p>
What is coding theory? 
</p>

<p>
Coding theory attempts to solve the problem of reliably transmitting
information from one location (or person), the <em>transmitter</em>, to a second
location (or person), the <em>receiver</em>. The information is transmitted across a
<em>channel</em>. The challenge is that this channel does not reliably transmit
information, but may introduce <em>noise</em>, or other distortions.
</p>

<p>
For example, the transmitter may want to send the message "HELLO WORLD". The
channel may introduce some noise, and the receiver then gets the message "HFLLP
VOPLD".
</p>

<p>
A typical way of tackling this problem is to recode the message to include some
<em>redundancy</em>. In this way, the channel probably cannot alter all the redundant
parts of the message, and so the receiver can reconstruct what has been sent.
This idea leads us to our first code.
</p>

<div id="2020-10-09: Repetition Codes-Binary Repetition Code"><h2 id="Binary Repetition Code" class="header"><a href="#2020-10-09: Repetition Codes-Binary Repetition Code">Binary Repetition Code</a></h2></div>

<p>
For the time being, I will simplify the type of messages that can be
transmitted. These messages will be sequences of binary digits, e.g. "100110".
The noise that the channel can introduce will mean that a bit can be flipped,
e.g. "101010" is a noisy version of the first binary message.
</p>

<p>
As we are talking about the channel, let us define <em>how</em> noise is added to a
message. We make the following assumptions to define a _binary symmetric
channel_:
</p>

<ul>
<li>
every binary digit in the message has the <em>same</em> probability, <em>p</em>, of being
  received in error; 

<li>
<em>p</em> is assumed to be less than 0.5 (this means we can assume the occurrence
  of one error is more likely than the occurrence of two errors); and

<li>
the chance of a transmitted 0 being received as an erroneous 1 is the same as
  that of a 1 being received as a 0.

</ul>
<p>
As an example, if we want to transmit the message "100110", what is the
probability that it will be received <em>without</em> errors? As <em>p</em> is the
probability that an error will occur in any bit, there is a (1-p) probability
that any individual bit will be received without error. 
</p>

<p>
For the six bits in the message, that means a (1-p)<sup><small>6</small></sup> probability that the
message is received correctly.
</p>

<p>
Therefore, for a message of length 6 bits, and p = 0.01 (i.e. noise affects 1
in 100 of the transmitted bits):
</p>

<ul>
<li>
Probability that the message is received correctly is 0.94

<li>
Or, probability that the message is received with one or more errors is 0.06

</ul>
<p>
The <em>binary repetition code</em> aims to reduce the error rate by sending each bit
several times. For example, instead of transmitting the bit 1, we will transmit
the bits 111, and instead of transmitting the bit 0, we will transmit the bits
000: the 111 and 000 are called the <em>codewords</em>.
</p>

<p>
The binary repetition code looks like this:
</p>

<ul>
<li>
Message bit 1 -&gt; 1 1 1 

<li>
Message bit 0 -&gt; 0 0 0

</ul>
<p>
So the message "100110" is encoded as: "111000000111111000".
</p>

<p>
What is the advantage of this? The main one is that we can <em>correct</em> a certain
amount of errors.
</p>

<p>
For example, if the message "101" is received, we can see that it is an error
(because "101" is not either of our codewords: "000" or "111"). However, it is
<em>closer</em> to "111" than it is to "000", and so we can <em>decode</em> the message into
the codeword "111" with a reasonable expectation that we have decoded it
correctly.
</p>

<p>
Notice how we rely on p being less than 0.5 here: 
</p>

<ul>
<li>
"101" is one error from "111" with probability (1-p)p(1-p), which is 0.0098
  for p=0.01.

<li>
"101" is two errors from "000" with probability p(1-p)p, which is 0.000099
  for p=0.01.

</ul>
<p>
The "closeness" of the codeword to either all 1s or all 0s is defined as its
<em>weight</em>, simply the sum of non-zero bits. If the weight is greater than half
of the number of bits, then it is closer to being all 1s than to all 0s.
</p>

<p>
The weight of "101" is 2, and 2 is greater than 3/2=1.5, so "101" is closer to "111".
</p>

<div id="2020-10-09: Repetition Codes-Word Error Probability"><h2 id="Word Error Probability" class="header"><a href="#2020-10-09: Repetition Codes-Word Error Probability">Word Error Probability</a></h2></div>

<p>
The <em>word error</em> probability defines the probability that a given codeword is
received in error. 
</p>

<p>
In the case without any coding, we were transmitting single bits: in effect,
the 1 and 0 bits are their own codewords. We could call this the redundant case
when the repetition code is of length 1. The word error probability in this
case is <em>p</em>, the probability that a given bit will be received in error.
</p>

<p>
In the case when the repetition code is of length 3, as above, the word error
can be calculated as follows. First, consider "000": which received messages
will be closer to "000" than "111"? These are "000", "001", "010", "100". So
the probability that "000" is received correctly can be calculated from the sum
of the probabilities that each of these would be received:
</p>

<ul>
<li>
P(000) = (1-p)(1-p)(1-p)

<li>
P(001) = (1-p)(1-p)p

<li>
P(010) = (1-p)p(1-p)

<li>
P(100) = p(1-p)(1-p)

</ul>
<p>
Which, with some rearranging, means the probability that "000" is received
correctly = (1-p)<sup><small>2</small></sup> (1+2p)
</p>

<p>
Thus, the word error probability is 1 - (1-p)<sup><small>2</small></sup> (1+2p) = 3p<sup><small>2</small></sup> - 2p<sup><small>3</small></sup>
</p>

<p>
(The same value is reached when considering "111", by symmetry.)
</p>

<p>
Some predictions, with p=0.01:
</p>

<ul>
<li>
repetition = 1, word error = 0.01

<li>
repetition = 3, word error = 0.000298

<li>
repetition = 5, word error = 0.000010

</ul>
<p>
Notice how the probability of receiving an error reduces as the amount of
repetition increases; the more redundancy there is in the message, the easier
it is to correct for noise.
</p>

<div id="2020-10-09: Repetition Codes-Simulation"><h2 id="Simulation" class="header"><a href="#2020-10-09: Repetition Codes-Simulation">Simulation</a></h2></div>

<p>
We can now write some code to implement this repetition code scheme, and try to
reproduce the predicted word error rates. Of course, any language can be used
here, but I'm using <a href="https://unicon.sourceforge.io">unicon</a>.
</p>

<p>
Messages will be stored as lists of integers, 0 or 1 as appropriate. We begin
with a procedure to calculate the weight of a given list of integers:
</p>

<pre>
<span style="color: #008800;font-weight: bold">procedure</span><span style=""> </span><span style="">weight</span><span style=""> </span><span style="">(</span><span style="">lst</span><span style="">)</span>
<span style="">  </span><span style="">total</span><span style=""> </span><span style="color: #0066bb;font-weight: bold">:=</span><span style=""> </span><span style="color: #0000dd;font-weight: bold">0</span>
<span style="">  </span><span style="color: #008800;font-weight: bold">every</span><span style=""> </span><span style="">total</span><span style=""> </span><span style="color: #0066bb;font-weight: bold">+:=</span><span style=""> </span><span style="color: #0066bb;font-weight: bold">!</span><span style="">lst</span>
<span style="">  </span><span style="color: #008800;font-weight: bold">return</span><span style=""> </span><span style="">total</span>
<span style="color: #008800;font-weight: bold">end</span>
</pre>

<p>
Oddly, I cannot find a standard way to compare two lists in unicon, so I added:
</p>

<pre>
<span style="color: #008800;font-weight: bold">procedure</span><span style=""> </span><span style="">equal_lists</span><span style=""> </span><span style="">(</span><span style="">lst1</span><span style="">,</span><span style=""> </span><span style="">lst2</span><span style="">)</span>
<span style="">  </span><span style="color: #008800;font-weight: bold">if</span><span style=""> </span><span style="color: #0066bb;font-weight: bold">*</span><span style="">lst1</span><span style=""> </span><span style="color: #0066bb;font-weight: bold">===</span><span style=""> </span><span style="color: #0066bb;font-weight: bold">*</span><span style="">lst2</span><span style=""> </span><span style="color: #008800;font-weight: bold">then</span><span style=""> </span><span style="color: #0066bb;font-weight: bold">{</span>
<span style="">    </span><span style="color: #008800;font-weight: bold">every</span><span style=""> </span><span style="">i</span><span style=""> </span><span style="color: #0066bb;font-weight: bold">:=</span><span style=""> </span><span style="">key</span><span style="">(</span><span style="">lst1</span><span style="">)</span><span style=""> </span><span style="color: #008800;font-weight: bold">do</span><span style=""> </span><span style="color: #0066bb;font-weight: bold">{</span>
<span style="">      </span><span style="color: #008800;font-weight: bold">if</span><span style=""> </span><span style="">lst1</span><span style="color: #0066bb;font-weight: bold">[</span><span style="">i</span><span style="color: #0066bb;font-weight: bold">]</span><span style=""> </span><span style="color: #0066bb;font-weight: bold">~===</span><span style=""> </span><span style="">lst2</span><span style="color: #0066bb;font-weight: bold">[</span><span style="">i</span><span style="color: #0066bb;font-weight: bold">]</span><span style=""> </span><span style="color: #008800;font-weight: bold">then</span><span style=""> </span><span style="color: #008800;font-weight: bold">fail</span><span style="">(</span><span style="">)</span>
<span style="">    </span><span style="color: #0066bb;font-weight: bold">}</span>
<span style="">  </span><span style="color: #0066bb;font-weight: bold">}</span><span style=""> </span><span style="color: #008800;font-weight: bold">else</span>
<span style="">    </span><span style="color: #008800;font-weight: bold">fail</span><span style="">(</span><span style="">)</span>

<span style="">  </span><span style="color: #008800;font-weight: bold">return</span>
<span style="color: #008800;font-weight: bold">end</span>
</pre>

<p>
The following two important procedures perform the actual encoding or decoding.
The first produces the repetition codeword, and the second uses the weight to
convert a codeword to its likely original value:
</p>

<pre>
<span style="color: #008800;font-weight: bold">procedure</span><span style=""> </span><span style="">encode</span><span style=""> </span><span style="">(</span><span style="">val</span><span style="">,</span><span style=""> </span><span style="">size</span><span style="">)</span>
<span style="">  </span><span style="">result</span><span style=""> </span><span style="color: #0066bb;font-weight: bold">:=</span><span style=""> </span><span style="color: #0066bb;font-weight: bold">[</span><span style="color: #0066bb;font-weight: bold">]</span>

<span style="">  </span><span style="color: #008800;font-weight: bold">every</span><span style=""> </span><span style="color: #0066bb;font-weight: bold">!</span><span style="">size</span><span style=""> </span><span style="color: #008800;font-weight: bold">do</span><span style=""> </span><span style="color: #0066bb;font-weight: bold">{</span>
<span style="">    </span><span style="">result</span><span style=""> </span><span style="color: #0066bb;font-weight: bold">:=</span><span style=""> </span><span style="">push</span><span style="">(</span><span style="">result</span><span style="">,</span><span style=""> </span><span style="">val</span><span style="">)</span>
<span style="">  </span><span style="color: #0066bb;font-weight: bold">}</span>

<span style="">  </span><span style="color: #008800;font-weight: bold">return</span><span style=""> </span><span style="">result</span>
<span style="color: #008800;font-weight: bold">end</span>

<span style="color: #008800;font-weight: bold">procedure</span><span style=""> </span><span style="">decode</span><span style=""> </span><span style="">(</span><span style="">val</span><span style="">,</span><span style=""> </span><span style="">size</span><span style="">)</span>
<span style="">  </span><span style="color: #008800;font-weight: bold">return</span><span style=""> </span><span style="color: #008800;font-weight: bold">if</span><span style=""> </span><span style="">weight</span><span style="">(</span><span style="">val</span><span style="">)</span><span style=""> </span><span style="color: #0066bb;font-weight: bold"><=</span><span style=""> </span><span style="">size</span><span style="color: #0066bb;font-weight: bold">/</span><span style="color: #0000dd;font-weight: bold">2</span><span style=""> </span><span style="color: #008800;font-weight: bold">then</span>
<span style="">    </span><span style="color: #0000dd;font-weight: bold">0</span>
<span style="">  </span><span style="color: #008800;font-weight: bold">else</span>
<span style="">    </span><span style="color: #0000dd;font-weight: bold">1</span>
<span style="color: #008800;font-weight: bold">end</span>
</pre>

<p>
The following two procedures will encode or decode an entire message. These are
probably not needed, given our simulation only tests individual codewords, but
they complete the code. 
</p>

<p>
The decoding of a message means dividing a perhaps long stream of binary digits
into blocks of the same size as our repetition code. e.g. with a code of
"111000000111111000", it must be divided into "111", "000", "000", "111", "111"
and "000", and then each individual block decoded.
</p>

<pre>
<span style="color: #008800;font-weight: bold">procedure</span><span style=""> </span><span style="">encode_msg</span><span style=""> </span><span style="">(</span><span style="">msg</span><span style="">,</span><span style=""> </span><span style="">size</span><span style="">)</span>
<span style="">  </span><span style="">result</span><span style=""> </span><span style="color: #0066bb;font-weight: bold">:=</span><span style=""> </span><span style="color: #0066bb;font-weight: bold">[</span><span style="color: #0066bb;font-weight: bold">]</span>

<span style="">  </span><span style="color: #008800;font-weight: bold">every</span><span style=""> </span><span style="">bit</span><span style=""> </span><span style="color: #0066bb;font-weight: bold">:=</span><span style=""> </span><span style="color: #0066bb;font-weight: bold">!</span><span style="">msg</span><span style=""> </span><span style="color: #008800;font-weight: bold">do</span><span style=""> </span><span style="color: #0066bb;font-weight: bold">{</span>
<span style="">    </span><span style="">result</span><span style=""> </span><span style="color: #0066bb;font-weight: bold">:=</span><span style=""> </span><span style="">result</span><span style=""> </span><span style="color: #0066bb;font-weight: bold">|||</span><span style=""> </span><span style="">encode</span><span style=""> </span><span style="">(</span><span style="">bit</span><span style="">,</span><span style=""> </span><span style="">size</span><span style="">)</span>
<span style="">  </span><span style="color: #0066bb;font-weight: bold">}</span>

<span style="">  </span><span style="color: #008800;font-weight: bold">return</span><span style=""> </span><span style="">result</span>
<span style="color: #008800;font-weight: bold">end</span>

<span style="color: #008800;font-weight: bold">procedure</span><span style=""> </span><span style="">decode_msg</span><span style=""> </span><span style="">(</span><span style="">msg</span><span style="">,</span><span style=""> </span><span style="">size</span><span style="">)</span>
<span style="">  </span><span style="">result</span><span style=""> </span><span style="color: #0066bb;font-weight: bold">:=</span><span style=""> </span><span style="color: #0066bb;font-weight: bold">[</span><span style="color: #0066bb;font-weight: bold">]</span>

<span style="">  </span><span style="color: #008800;font-weight: bold">every</span><span style=""> </span><span style="">i</span><span style="color: #0066bb;font-weight: bold">:=</span><span style=""> </span><span style="color: #0000dd;font-weight: bold">1</span><span style=""> </span><span style="color: #008800;font-weight: bold">to</span><span style=""> </span><span style="color: #0066bb;font-weight: bold">*</span><span style="">msg</span><span style=""> </span><span style="color: #008800;font-weight: bold">by</span><span style=""> </span><span style="">size</span><span style=""> </span><span style="color: #008800;font-weight: bold">do</span><span style=""> </span><span style="color: #0066bb;font-weight: bold">{</span>
<span style="">    </span><span style="">result</span><span style=""> </span><span style="color: #0066bb;font-weight: bold">:=</span><span style=""> </span><span style="">put</span><span style=""> </span><span style="">(</span><span style="">result</span><span style="">,</span><span style=""> </span><span style="">decode</span><span style=""> </span><span style="">(</span><span style="">msg</span><span style="color: #0066bb;font-weight: bold">[</span><span style="">i</span><span style="color: #0066bb;font-weight: bold">+:</span><span style="">size</span><span style="color: #0066bb;font-weight: bold">]</span><span style="">,</span><span style=""> </span><span style="">size</span><span style="">)</span><span style="">)</span>
<span style="">  </span><span style="color: #0066bb;font-weight: bold">}</span>

<span style="">  </span><span style="color: #008800;font-weight: bold">return</span><span style=""> </span><span style="">result</span>
<span style="color: #008800;font-weight: bold">end</span>
</pre>

<p>
The <em>transmit</em> procedure takes the message and randomly inverts each bit, using
the given <em>p</em>:
</p>

<pre>
<span style="color: #008800;font-weight: bold">procedure</span><span style=""> </span><span style="">transmit</span><span style=""> </span><span style="">(</span><span style="">msg</span><span style="">)</span>
<span style="">  </span><span style="">p</span><span style=""> </span><span style="color: #0066bb;font-weight: bold">:=</span><span style=""> </span><span style="color: #0000dd;font-weight: bold">0.01</span><span style=""> </span><span style="color: #888888; font-style: italic"># symbol error probability</span>

<span style="">  </span><span style="color: #008800;font-weight: bold">every</span><span style=""> </span><span style="">i</span><span style=""> </span><span style="color: #0066bb;font-weight: bold">:=</span><span style=""> </span><span style="">key</span><span style="">(</span><span style="">msg</span><span style="">)</span><span style=""> </span><span style="color: #008800;font-weight: bold">do</span><span style=""> </span><span style="color: #0066bb;font-weight: bold">{</span>
<span style="">    </span><span style="color: #008800;font-weight: bold">if</span><span style=""> </span><span style="color: #0066bb;font-weight: bold">?</span><span style="color: #0000dd;font-weight: bold">0</span><span style=""> </span><span style="color: #0066bb;font-weight: bold"><</span><span style=""> </span><span style="">p</span><span style=""> </span><span style="color: #008800;font-weight: bold">then</span><span style=""> </span><span style="">msg</span><span style="color: #0066bb;font-weight: bold">[</span><span style="">i</span><span style="color: #0066bb;font-weight: bold">]</span><span style=""> </span><span style="color: #0066bb;font-weight: bold">:=</span><span style=""> </span><span style="color: #0000dd;font-weight: bold">1</span><span style="color: #0066bb;font-weight: bold">-</span><span style="">msg</span><span style="color: #0066bb;font-weight: bold">[</span><span style="">i</span><span style="color: #0066bb;font-weight: bold">]</span><span style=""> </span><span style="color: #888888; font-style: italic"># add noise</span>
<span style="">  </span><span style="color: #0066bb;font-weight: bold">}</span>

<span style="">  </span><span style="color: #008800;font-weight: bold">return</span><span style=""> </span><span style="">msg</span>
<span style="color: #008800;font-weight: bold">end</span>
</pre>

<p>
Finally, the <em>main</em> procedure runs the simulation. It loops through the three
repetition sizes and transmits a random codeword as the message for N
iterations, checking the accuracy of the decoded message. The procedure reports
on the overall word error probability for each repetition size:
</p>

<pre>
<span style="color: #008800;font-weight: bold">procedure</span><span style=""> </span><span style="">main</span><span style=""> </span><span style="">(</span><span style="">arglist</span><span style="">)</span>
<span style="">  </span><span style="">N</span><span style=""> </span><span style="color: #0066bb;font-weight: bold">:=</span><span style=""> </span><span style="color: #0000dd;font-weight: bold">1000000</span><span style=""> </span><span style="color: #888888; font-style: italic"># number of iterations</span>

<span style="">  </span><span style="color: #008800;font-weight: bold">every</span><span style=""> </span><span style="">size</span><span style=""> </span><span style="color: #0066bb;font-weight: bold">:=</span><span style=""> </span><span style="color: #0066bb;font-weight: bold">!</span><span style="color: #0066bb;font-weight: bold">[</span><span style="color: #0000dd;font-weight: bold">1</span><span style="">,</span><span style="color: #0000dd;font-weight: bold">3</span><span style="">,</span><span style="color: #0000dd;font-weight: bold">5</span><span style="color: #0066bb;font-weight: bold">]</span><span style=""> </span><span style="color: #008800;font-weight: bold">do</span><span style=""> </span><span style="color: #0066bb;font-weight: bold">{</span>
<span style="">    </span><span style="">correct</span><span style=""> </span><span style="color: #0066bb;font-weight: bold">:=</span><span style=""> </span><span style="color: #0000dd;font-weight: bold">0</span>

<span style="">    </span><span style="color: #008800;font-weight: bold">every</span><span style=""> </span><span style="color: #0066bb;font-weight: bold">!</span><span style="">N</span><span style=""> </span><span style="color: #008800;font-weight: bold">do</span><span style=""> </span><span style="color: #0066bb;font-weight: bold">{</span>
<span style="">      </span><span style="">msg</span><span style=""> </span><span style="color: #0066bb;font-weight: bold">:=</span><span style=""> </span><span style="color: #0066bb;font-weight: bold">[</span><span style=""> </span><span style="color: #0066bb;font-weight: bold">?</span><span style="color: #0066bb;font-weight: bold">[</span><span style="color: #0000dd;font-weight: bold">0</span><span style="">,</span><span style=""> </span><span style="color: #0000dd;font-weight: bold">1</span><span style="color: #0066bb;font-weight: bold">]</span><span style=""> </span><span style="color: #0066bb;font-weight: bold">]</span><span style="">  </span><span style="color: #888888; font-style: italic"># msg is a single bit</span>
<span style="">      </span><span style="">received</span><span style=""> </span><span style="color: #0066bb;font-weight: bold">:=</span><span style=""> </span><span style="">decode_msg</span><span style=""> </span><span style="">(</span><span style="">transmit</span><span style=""> </span><span style="">(</span><span style="">encode_msg</span><span style=""> </span><span style="">(</span><span style="">msg</span><span style="">,</span><span style=""> </span><span style="">size</span><span style="">)</span><span style="">)</span><span style="">,</span><span style=""> </span><span style="">size</span><span style="">)</span>
<span style="">      </span><span style="color: #008800;font-weight: bold">if</span><span style=""> </span><span style="">equal_lists</span><span style="">(</span><span style="">msg</span><span style="">,</span><span style=""> </span><span style="">received</span><span style="">)</span><span style=""> </span><span style="color: #008800;font-weight: bold">then</span><span style=""> </span><span style="">correct</span><span style=""> </span><span style="color: #0066bb;font-weight: bold">+:=</span><span style=""> </span><span style="color: #0000dd;font-weight: bold">1</span>
<span style="">    </span><span style="color: #0066bb;font-weight: bold">}</span>

<span style="">    </span><span style="">write</span><span style=""> </span><span style="">(</span><span style="color: #dd2200;background-color: #fff0f0">"Size           = "</span><span style="">,</span><span style=""> </span><span style="">size</span><span style="">)</span>
<span style="">    </span><span style="">write</span><span style=""> </span><span style="">(</span><span style="color: #dd2200;background-color: #fff0f0">"N              = "</span><span style="">,</span><span style=""> </span><span style="">N</span><span style="">)</span>
<span style="">    </span><span style="">write</span><span style=""> </span><span style="">(</span><span style="color: #dd2200;background-color: #fff0f0">"Number correct = "</span><span style="">,</span><span style=""> </span><span style="">correct</span><span style="">)</span>
<span style="">    </span><span style="">write</span><span style=""> </span><span style="">(</span><span style="color: #dd2200;background-color: #fff0f0">"Perr(C)        = "</span><span style="">,</span><span style=""> </span><span style="color: #0000dd;font-weight: bold">1</span><span style="color: #0066bb;font-weight: bold">-</span><span style="">real</span><span style="">(</span><span style="">correct</span><span style="">)</span><span style="color: #0066bb;font-weight: bold">/</span><span style="">N</span><span style="">)</span>
<span style="">    </span><span style="">write</span><span style=""> </span><span style="">(</span><span style="">)</span>
<span style="">  </span><span style="color: #0066bb;font-weight: bold">}</span>
<span style="color: #008800;font-weight: bold">end</span>
</pre>

<p>
Results are reasonably close, for 1 million iterations (comment added to show expected value):
</p>

<pre>
$ unicon -C codes.icn
$ ./codes
Size           = 1
N              = 1000000
Number correct = 990027
Perr(C)        = 0.00997300000000001    # expected 0.01

Size           = 3
N              = 1000000
Number correct = 999710
Perr(C)        = 0.0002900000000000125  # expected 0.000298

Size           = 5
N              = 1000000
Number correct = 999992
Perr(C)        = 8.000000000008001e-06  # =0.000008 expected 0.000010
</pre>

    </div>
    <hr>
    <small>Page from Peter's <a href="../index.html">Scrapbook</a>, output from a <a href="http://vimwiki.github.io/">VimWiki</a> on 2024-01-29.</small>
</body>
</html>
