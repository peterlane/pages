<html>
<head>
    <link rel="Stylesheet" type="text/css" href="../style.css" />
    <title>2024-02-26</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
</head>
<body>
    <div class="content">
    
<div id="2024-02-26: Functional Programming in Fantom"><h1 id="2024-02-26: Functional Programming in Fantom" class="header"><a href="#2024-02-26: Functional Programming in Fantom">2024-02-26: Functional Programming in Fantom</a></h1></div>

<p>
I recently took a look at the book "Functional Programming in Java", 2nd
Edition, by Venkat Subramaniam.  The focus of the book is on Java's lambda
expressions and streams library, which leads me to wonder: what does Fantom
have to say on these ideas?  Fantom has
<a href="https://fantom.org/doc/docLang/Closures">closures</a> and rich collection
libraries. I thought it would be interesting to review Fantom's abilities in
this area, using the early part of the above book as a guide; the later part of
the book considers more general design questions or Java-specific material,
some of which is also applicable to Fantom code.
</p>

<div id="2024-02-26: Functional Programming in Fantom-Illustrating Anonymous Function Arguments"><h2 id="Illustrating Anonymous Function Arguments" class="header"><a href="#2024-02-26: Functional Programming in Fantom-Illustrating Anonymous Function Arguments">Illustrating Anonymous Function Arguments</a></h2></div>

<p>
The first example from the book aims to highlight the difference between a
procedural and more functional solution to the same problem: take a list of
prices, and then calculate the total of the discounted rate of items valued
over 20:
</p>

<p>
A procedural Fantom version may look like this:
</p>

<pre fantom>
class Main
{
  static Void main(Str[] args)
  {
    prices := Int[10, 30, 17, 20, 18, 45, 12]

    totalOfDiscountedPrices := 0.0f
    for (i := 0; i &lt; prices.size; i += 1) 
    {
      if (prices[i] &gt; 20)
      {
        totalOfDiscountedPrices += prices[i] * 0.9f
      }
    }

    echo("Total of discounted prices: $totalOfDiscountedPrices")
  }
}
</pre>

<p>
The functional version relies on common operations working directly on lists,
using a closure (an anonymous function) as an argument to specify the required
behaviour of each list method for this problem:
</p>

<pre fantom>
  prices := Int[10, 30, 17, 20, 18, 45, 12]

  totalOfDiscountedPrices := prices
    .findAll { it &gt; 20 }                                      // &lt;1&gt;
    .map |Int x -&gt; Float| { x * 0.9f }                        // &lt;2&gt;
    .reduce(0.0f) |Float r, Float v-&gt;Float| { return r + v }  // &lt;3&gt;

  echo("Total of discounted prices: $totalOfDiscountedPrices")
</pre>
<ol>
<li>
The "findAll" method tests each item against an 
   <a href="https://fantom.org/doc/docLang/Closures#itBlocks">it-block</a>.

<li>
Types are explicitly provided to the "map" function's argument, to ensure 
   the list is converted to one of the expected type.

<li>
Finally, "reduce" collapses a list of items into a single value, based on 
   the given function.

</ol>
<p>
As we can see, the <em>intent</em> of the code is somewhat clearer from the top-level
methods: <code>findAll</code> finds prices with the given condition, <code>map</code> converts 
items using a formula, and <code>reduce</code> combines the items into a single result.
</p>

<p>
Fantom's syntax is very clean here: methods do not need parentheses if there
are no arguments, and, when the last argument is a function, it can be written
"outside" the usual argument list - see <code>reduce</code>, which takes the initial 
item and the reducer function as its two arguments.
</p>

<p>
You may be concerned about <em>efficiency</em>: I expect the fact that each Fantom
operation creates a new list will mean the functional-style will be 
inherently less efficient, which may be something you need to watch.
</p>

<div id="2024-02-26: Functional Programming in Fantom-First-Class Functions"><h2 id="First-Class Functions" class="header"><a href="#2024-02-26: Functional Programming in Fantom-First-Class Functions">First-Class Functions</a></h2></div>

<p>
A typical test of a language's functional credentials is to see if functions
are "first-class" values: in Fantom's case, this is true.  What do we mean by
"first-class" values? Definitions vary a little, but the key properties are
that you can:
</p>

<ul>
<li>
<em>reference</em> a function with a variable;

<li>
<em>pass</em> a function to another function (or method) as a parameter; and

<li>
<em>return</em> a function from another function (or method) as a result.

</ul>
<p>
Here's an example of this. Two functions are created and given names <code>sum</code> and
<code>diff</code>. These can then be passed to another function (method) called <code>doCalc</code>,
which applies the passed function to some values and returns a result. The code
also illustrates how the functions can be created directly at the time of the
call to <code>doCalc</code>. The <code>makeAdder</code> method accepts one argument, and then returns
a function which accepts a second argument and returns the sum of the two
arguments.
</p>

<pre fantom>
class ExampleClosure
{
  static Void main(Str[] args)
  {
    sum := |Int a, Int b -&gt; Int| { return a + b }         // &lt;1&gt;
    diff := |Int a, Int b -&gt; Int| { return a - b }

    echo("Sum:  " + doCalc(10, 2, sum))                   // &lt;2&gt;
    echo("Diff: " + doCalc(10, 2, diff))
    echo("Times: " + doCalc(10, 2, |Int a, Int b -&gt; Int| { return a * b }))
    echo("Divide: " + doCalc(10, 2) |Int a, Int b -&gt; Int| { return a / b })
   
    add1 := makeAdder(1)                                  // &lt;3&gt;
    add2 := makeAdder(2)
    echo("Add 1: " + add1(10))                            // &lt;4&gt;
    echo("Add 2: " + add2(10))
  }

  // This method accepts a function of two numbers, and returns the result of 
  // applying this function.
  static Int doCalc(Int a, Int b, |Int,Int -&gt; Int| calc)  // &lt;5&gt;
  {
    return calc(a, b)                                     // &lt;6&gt;
  }
  
  // This method takes a number and returns a function which uses that number.
  static |Int -&gt; Int| makeAdder(Int x)                    // &lt;7&gt;
  {
    return |Int y -&gt; Int| { return x + y }                // &lt;8&gt;
  }
}
</pre>
<ol>
<li>
Create a function and assign it to a variable, <code>sum</code>.

<li>
Pass the function as a parameter to the <code>doCalc</code> method. Notice how the 
   function can be passed using a variable, directly as a value, and that the 
   function argument can be passed from outside the argument parentheses.

<li>
Call a method which returns a function, and store the result.

<li>
Apply a function stored in a variable reference.

<li>
Last parameter is a function value.

<li>
Applies the provided function value to compute a result.

<li>
Notice the return type: returns a function value.

<li>
Create and return a function value.

</ol>
<p>
Functions are "closures", which means they can reference variables in their
environment. Hence, this technique for adding the numbers in a list:
</p>

<pre fantom>
items := Int[1,2,3,4,5]     // &lt;1&gt;
sum := 0                    // &lt;2&gt;
items.each |Int x|          // &lt;3&gt;
{
  sum += x                  // &lt;4&gt;
}
echo("Sum is: $sum")        // &lt;5&gt;
</pre>
<ol>
<li>
Create a list to iterate over.

<li>
A local variable to hold the result.

<li>
Pass a closure to the <code>each</code> method defined on the <code>List</code> collection.

<li>
Within the closure, modify the local variable <code>sum</code>.

<li>
Show the final result, which should be 15.

</ol>
<div id="2024-02-26: Functional Programming in Fantom-Lists"><h2 id="Lists" class="header"><a href="#2024-02-26: Functional Programming in Fantom-Lists">Lists</a></h2></div>

<p>
The <a href="https://fantom.org/doc/sys/List">List</a> is an ordered collection of
items, indexed by an integer.
</p>

<p>
There are five kinds of operations supported on lists which accept functions as
one of their parameters, distinguished largely by the kind of result obtained
from the function when applied to all items in the list in turn:
</p>

<table>
<thead>
<tr>
<th>
Kind of Operation
</th>
<th>
Kind of Result
</th>
<th>
Methods on List
</th>
</tr>
</thead>
<tbody>
<tr>
<td>
collapse to value
</td>
<td>
return a single overall value from function results
</td>
<td>
<code>all</code> <code>any</code> <code>join</code> <code>max</code> <code>min</code> <code>reduce</code>
</td>
</tr>
<tr>
<td>
iteration
</td>
<td>
applies function to all, or sublist of, items
</td>
<td>
<code>each</code> <code>eachr</code> <code>eachNotNull</code> <code>eachRange</code>
</td>
</tr>
<tr>
<td>
search
</td>
<td>
return a new value or list of list values based on function results
</td>
<td>
<code>binaryFind</code> <code>binarySearch</code> <code>eachWhile</code> <code>eachrWhile</code> <code>exclude</code> <code>find</code> <code>findAll</code> <code>findIndex</code>
</td>
</tr>
<tr>
<td>
split
</td>
<td>
return multiple results based on function results
</td>
<td>
<code>groupBy</code> <code>groupByInto</code>
</td>
</tr>
<tr>
<td>
transform
</td>
<td>
return a list of values based on function results
</td>
<td>
<code>flatMap</code> <code>map</code> <code>mapNotNull</code> <code>sort</code> <code>sortr</code>
</td>
</tr>
</tbody>
</table>

<div id="2024-02-26: Functional Programming in Fantom-Lists-Collapse to Value"><h3 id="Collapse to Value" class="header"><a href="#2024-02-26: Functional Programming in Fantom-Lists-Collapse to Value">Collapse to Value</a></h3></div>

<p>
Methods in this group apply a function to all items in the list, but
"collapse" the results into a single value. 
</p>

<p>
The basic method is <code>reduce</code> - in fact, this method is so powerful you can
define most, if not all, the other methods described on this page in terms of
it.
</p>

<p>
The <code>reduce</code> method takes two arguments: an initial value, which can be any
kind of value, and a function. The idea is that the function combines a partial
result with the next list item to obtain a new partial result: starting from
the initial value, the function is applied to each list item in turn, finally
returning the overall result. To achieve this, the function itself takes two
arguments: something of the same type as the initial value (the partial
result), and the next item in the list.  The function returns something of the
same type as the initial value. 
</p>

<p>
For example, consider how to add up a list of integers: the initial value will
be 0, and the function will take the current sum, and return a new sum
consisting of the current sum added to the next item in the list:
</p>

<pre>
fansh&gt; [1,2,3,4,5].reduce(0) |Int sum, Int item -&gt; Int| { return sum + item }
15
</pre>

<p>
The initial value can be a more complex type: let's consider returning a map of 
each integer linked to its square:
</p>

<pre>
fansh&gt; [1,2,3,4,5].reduce(Int:Int[:]) |Int:Int squares, Int item -&gt; Int:Int| { return squares[item] = item*item }
[1:1, 2:4, 3:9, 4:16, 5:25]
</pre>

<p>
The following methods are special applications of <code>reduce</code>:
</p>

<ul>
<li>
<code>all</code> - returns true if the given function returns true for all items in the list
<pre>
fansh&gt; [1,2,3,4,5].all { isOdd }
false
fansh&gt; [1,2,3,4,5].all { it &lt; 6 }
true
</pre>

<li>
<code>any</code> - returns true if the given function returns true for at least one item in the list
<pre>
fansh&gt; [1,2,3,4,5].any { isOdd }
true
</pre>

<li>
<code>join</code> - returns a string formed by joining a string representation of each
  item using given separator. If a function is provided, it is used to provide
  each item's string, otherwise <code>toStr</code> is called.
<pre>
fansh&gt; [1,2,3,4,5].join(" ")
1 2 3 4 5
fansh&gt; [1,2,3,4,5].join(",") { "&lt; $it &gt;" }
&lt; 1 &gt;,&lt; 2 &gt;,&lt; 3 &gt;,&lt; 4 &gt;,&lt; 5 &gt;
</pre>

<li>
<code>max</code> - returns the largest element in the list, using the given function to
  define "largest". Notice in this case the function receives two items to 
  compare, and returns the usual comparator values of -1, 0, or 1.
<pre>
fansh&gt; [1,-3,-7,4,-2].max |Int a, Int b -&gt; Int| {return a.abs &lt;=&gt; b.abs }
-7
</pre>

<li>
<code>min</code> - returns the smallest element in the list, using the given function to
  define "largest". Notice in this case the function receives two items to 
  compare, and returns the usual comparator values of -1, 0, or 1.
<pre>
fansh&gt; [1,-3,-7,4,-2].min |Int a, Int b -&gt; Int| {return a.abs &lt;=&gt; b.abs }
1
</pre>

</ul>
<div id="2024-02-26: Functional Programming in Fantom-Lists-Iteration"><h3 id="Iteration" class="header"><a href="#2024-02-26: Functional Programming in Fantom-Lists-Iteration">Iteration</a></h3></div>

<p>
Iteration applies the given function to all, or a sublist of, the items in the
list. There is no specific return value, and the function is assumed to
generate side-effects. The function receives two arguments: the item in the
list and the item's index position in the list.
</p>

<p>
The basic method here is <code>each</code>, which applies a given function to every item
in the list:
</p>

<pre>
fansh&gt; s := Str["a", "b", "c", "d"]
[a, b, c, d]
fansh&gt; s.each |Str item, Int index| { echo("Item $item at $index") }
Item a at 0
Item b at 1
Item c at 2
Item d at 3
</pre>

<p>
As Fantom allows us to use functions with less than the expected number of arguments,
our call to <code>each</code> can be restricted to just the item, and this can be the focus of 
an "it-block":
</p>

<pre>
fansh&gt; s.each |Str item| { echo("Item is $item") }
Item is a
Item is b
Item is c
Item is d
fansh&gt; s.each { echo("Item is $it") }
Item is a
Item is b
Item is c
Item is d
</pre>

<p>
Two small variations of <code>each</code> are <code>eachr</code> and <code>eachNotNull</code>: 
</p>

<ul>
<li>
<code>eachr</code> iterates through the list in reverse order:
<pre>
fansh&gt; s.eachr |Str item, Int index| { echo("Item $item at $index") }
Item d at 3
Item c at 2
Item b at 1
Item a at 0
</pre>

<li>
<code>eachNotNull</code> iterates forwards, skipping over any null items:
<pre>
fansh&gt; t := Str?["a", null, "b", "c", null]
[a, null, b, c, null]
fansh&gt; t.each { echo(it) }
a
null
b
c
null
fansh&gt; t.eachNotNull { echo(it) }
a
b
c
</pre>

</ul>
<p>
The final method restricts iteration to a sub-part of the list:
</p>

<ul>
<li>
<code>eachRange</code> iterates over a given range of indices:
<pre>
fansh&gt; t.eachRange(2..3) { echo(it) }
b
c
</pre>

</ul>
<div id="2024-02-26: Functional Programming in Fantom-Lists-Search"><h3 id="Search" class="header"><a href="#2024-02-26: Functional Programming in Fantom-Lists-Search">Search</a></h3></div>

<p>
Search methods use their function to locate one or more items in the list.
</p>

<p>
The two basic methods here are <code>find</code> and <code>findAll</code>. They each use a boolean
function accepting two arguments, the usual item in the list and its index
position. However, the first returns the <em>first</em> item in the list where the
function returns true, and the second returns a new list of <em>all</em> items in the
list where the function returns true.
</p>

<pre>
fansh&gt; [1,2,3,4,5].find { isEven }
2
fansh&gt; [1,2,3,4,5].findAll { isEven }
[2, 4]
fansh&gt; [1,2,3,4,5].find { it &gt; 6 }
null
fansh&gt; [1,2,3,4,5].findAll { it &gt; 6 }
[,]
</pre>

<p>
The following two methods are variations on the basic methods:
</p>

<ul>
<li>
<code>exclude</code> is the inverse of <code>findAll</code>, returning all items for which the
  function returns false
<pre>
fansh&gt; [1,2,3,4,5].exclude { isEven }
[1, 3, 5]
</pre>

<li>
<code>findIndex</code> is like <code>find</code> but returns the found item's index instead of the
  item itself
<pre>
fansh&gt; [1,2,3,4,5].findIndex { isEven }
1
fansh&gt; [1,2,3,4,5].findIndex { it &gt; 6 }
null
</pre>

</ul>
<p>
The following two methods are also related to <code>find</code>, but they search on the
function's computed value, stopping when a non-null computed value is found:
</p>

<ul>
<li>
<code>eachWhile</code> iterates through a list, returning the first non-null value 
  returned by the function.

<li>
<code>eachrWhile</code> works in the same way, but starts iterating from the end of the
  list.
<pre>
fansh&gt; tester := |Int x -&gt; Int?| { if (x.isOdd) return x; else return null; }
|sys::Int-&gt;sys::Int?|
fansh&gt; tester(3)
3
fansh&gt; tester(2)
null
fansh&gt; [2,4,1,3,2].eachWhile(tester)
1
fansh&gt; [2,4,6,8].eachWhile(tester)
null
fansh&gt; [2,4,1,3,2].eachrWhile(tester)
3
</pre>

</ul>
<p>
Finally, in the case where your list is ordered, there are two efficient search 
algorithms.
</p>

<ul>
<li>
<code>binaryFind</code> - given a function accepting a list item and its index, returns a 
  comparator value indicating if the given item is less, equal or greater than
  the target. The final result is the index of the found item, or a negative
  number if the target is not found.

</ul>
<p>
In the following example, the first attempt looks for the position of "3", and
returns its index of 1. The second attempt looks for the position of "4", but 
fails to find it, returning -3.
</p>

<pre>
fansh&gt; [1,3,5,7,9].binaryFind |Int item-&gt;Int| { return 3 &lt;=&gt; item }
1
fansh&gt; [1,3,5,7,9].binaryFind |Int item-&gt;Int| { return 4 &lt;=&gt; item }
-3
</pre>

<ul>
<li>
<code>binarySearch</code> - searches for a given target value, with an optional comparator
  function specifying the order of the list

</ul>
<p>
In the following example, the list of strings is ordered by size, so the
comparator function to compare items by size is provided:
</p>

<pre>
fansh&gt; ["a", "g", "cat", "badger"].binarySearch("cat")
-2
fansh&gt; ["a", "g", "cat", "badger"].binarySearch("cat") |Str a, Str b-&gt;Int| { return a.size &lt;=&gt; b.size }
2
</pre>

<div id="2024-02-26: Functional Programming in Fantom-Lists-Split"><h3 id="Split" class="header"><a href="#2024-02-26: Functional Programming in Fantom-Lists-Split">Split</a></h3></div>

<p>
The "split" group of methods divide a list into sublists based on the results
of a function: all items in the list for which the function produces the 
same value are placed into the same group. The groups are stored in a map, 
with the function value as the key and list of items as its value.
</p>

<p>
The basic method here is <code>groupBy</code> - as with <code>each</code>, the provided function 
is passed two arguments, the item in the list and its index:
</p>

<pre>
fansh&gt; [1,2,3,4,5].groupBy { isOdd }
[false:[2, 4], true:[1, 3, 5]]
</pre>

<p>
The <code>isOdd</code> method is called on each item in the list: it returns <code>false</code> for
even numbers and <code>true</code> for odd numbers, and you can see those items in the
returned map.
</p>

<p>
An extension is <code>groupByInto</code>, which extends an existing map - in this example,
we group words by their size:
</p>

<pre>
fansh&gt; results := ["ape", "bear", "cat", "deer"].groupBy { size }
[3:[ape, cat], 4:[bear, deer]]
fansh&gt; ["elephant", "fox", "gorilla", "hare"].groupByInto(results) { size }
[3:[ape, cat, fox], 4:[bear, deer, hare], 7:[gorilla], 8:[elephant]]
</pre>

<p>
Notice from the call to <code>groupByInto</code> that "fox" is added to the group for 3,
"hare" to the group for 4, and new groups are added.
</p>

<div id="2024-02-26: Functional Programming in Fantom-Lists-Transform"><h3 id="Transform" class="header"><a href="#2024-02-26: Functional Programming in Fantom-Lists-Transform">Transform</a></h3></div>

<p>
The "transform" group of methods apply a function to each item in the list and
return a list of results. 
</p>

<p>
There are two sets of methods in this group: the first return new lists of
values using the results of a function, which receives each item in the list
and its index position in turn; the second rearrange (sort) the items in the
list using an optional comparator function.
</p>

<p>
The basic function for this group is <code>map</code>, which returns a new list of values:
</p>

<pre>
fansh&gt; [1,2,3,4,5].map { it*it }
[1, 4, 9, 16, 25]
fansh&gt; [1,2,3,4,5].map |Int item, Int index-&gt;Str| { return "[$index] =&gt; $item" }
[[0] =&gt; 1, [1] =&gt; 2, [2] =&gt; 3, [3] =&gt; 4, [4] =&gt; 5]
</pre>

<p>
Two variations make minor modifications to the list of returned values:
</p>

<ul>
<li>
<code>flatMap</code> is the same as <code>map</code> except that any list results from <code>map</code> are
  "flattened" - placing their items directly into the result list
<pre>
fansh&gt; ["abc", "def"].map { chars }
[[97, 98, 99], [100, 101, 102]]
fansh&gt; ["abc", "def"].flatMap { chars }
[97, 98, 99, 100, 101, 102]
</pre>

<li>
<code>mapNotNull</code> is the same as <code>map</code> except that null results are excluded
<pre>
fansh&gt; tester := |Int x -&gt; Int?| { if (x.isOdd) return x; else return null; }
|sys::Int-&gt;sys::Int?|
fansh&gt; [1,2,3,4,5].map(tester)
[1, null, 3, null, 5]
fansh&gt; [1,2,3,4,5].mapNotNull(tester)
[1, 3, 5]
</pre>

</ul>
<p>
Two methods destructively rearrange the items in a list: <code>sort</code> and <code>sortr</code>.
The latter sorts the list in reverse order. These methods take an optional
function to define the comparator:
</p>

<pre>
fansh&gt; ["badger", "cat", "hare", "elephant"].sort
[badger, cat, elephant, hare]
fansh&gt; ["badger", "cat", "hare", "elephant"].sort |Str a, Str b-&gt;Int| { return a.size &lt;=&gt; b.size }
[cat, hare, badger, elephant]
fansh&gt; ["badger", "cat", "hare", "elephant"].sortr
[hare, elephant, cat, badger]
fansh&gt; ["badger", "cat", "hare", "elephant"].sortr |Str a, Str b-&gt;Int| { return a.size &lt;=&gt; b.size }
[elephant, badger, hare, cat]
</pre>

<div id="2024-02-26: Functional Programming in Fantom-Maps"><h2 id="Maps" class="header"><a href="#2024-02-26: Functional Programming in Fantom-Maps">Maps</a></h2></div>

<p>
The <a href="https://fantom.org/doc/sys/Map">Map</a> is a collection of values, indexed
by keys.
</p>

<p>
As with the List, there are various kinds of operations supported on maps which
accept functions as one of their parameters. There is no "split" style method,
but there are analogous examples of the other four kinds of operations:
</p>

<table>
<thead>
<tr>
<th>
Kind of Operation
</th>
<th>
Kind of Result
</th>
<th>
Methods on Map
</th>
</tr>
</thead>
<tbody>
<tr>
<td>
collapse to value
</td>
<td>
return a single overall value from function results
</td>
<td>
<code>all</code> <code>any</code> <code>join</code> <code>reduce</code>
</td>
</tr>
<tr>
<td>
iteration
</td>
<td>
applies function to all, or sublist of, values
</td>
<td>
<code>each</code>
</td>
</tr>
<tr>
<td>
search
</td>
<td>
return a new value or list of list values based on function results
</td>
<td>
<code>eachWhile</code> <code>exclude</code> <code>find</code> <code>findAll</code>
</td>
</tr>
<tr>
<td>
transform
</td>
<td>
return a new list of values based on function results
</td>
<td>
<code>map</code> <code>mapNotNull</code>
</td>
</tr>
</tbody>
</table>

<p>
These methods are essentially identical to those on lists, but use (value,key)
parameter pairs for their function in place of (value,index). Methods like
<code>findAll</code> and <code>map</code> now return maps, preserving the keys from the original map.
</p>

<p>
The following three examples illustrate the similarities and differences:
</p>

<pre>
fansh&gt; [1:"one",2:"two",3:"three"].each |Str value, Int key| { echo("$key =&gt; $value") }
1 =&gt; one
2 =&gt; two
3 =&gt; three
fansh&gt; [1:"one",2:"two",3:"three"].findAll |Str value, Int key-&gt;Bool| { key.isOdd }
[1:one, 3:three]
fansh&gt; [1:"one",2:"two",3:"three"].map |Str value, Int key -&gt; Int| { value.size }
[1:3, 2:3, 3:5]
</pre>

<p>
There are three methods specific to maps which take functions as one of their arguments.
These are:
</p>

<ul>
<li>
<code>addList</code> - given a list of values, the values are added to the map using as
  key either themselves or the result of applying the optional function to each
  value. If a key already exists in the list, then the method fails.
<pre>
fansh&gt; m := [1:"one",2:"two",3:"three"]
[1:one, 2:two, 3:three]
fansh&gt; m.addList(["four", "eleven", "thirteen"]) |Str value-&gt;Int| { value.size }
[1:one, 2:two, 3:three, 4:four, 6:eleven, 8:thirteen]
fansh&gt; m.addList(["ten"]) |Str value-&gt;Int| { value.size }
sys::ArgErr: Key already mapped: 3
  fan.sys.Map.add (Map.java:165)
  fan.sys.Map.addList (Map.java:247)
</pre>

<li>
<code>getOrAdd</code> - extends <code>get</code> so that any missing keys are added using the value
  generated by the given function
<pre>
fansh&gt; n := Str:Int[:]
[:]
fansh&gt; n.getOrAdd("one") |Str key-&gt;Int| { key.size }
3
fansh&gt; n.getOrAdd("four") |Str key-&gt;Int| { key.size }
4
fansh&gt; n
[four:4, one:3]
</pre>

</ul>
  
<ul>
<li>
<code>setList</code> - is the same as <code>addList</code>, except it overwrites any existing mapping.
<pre>
fansh&gt; m := [1:"one",2:"two",3:"three"]
[1:one, 2:two, 3:three]
fansh&gt; m.setList(["four", "eleven", "thirteen"]) |Str value-&gt;Int| { value.size }
[1:one, 2:two, 3:three, 4:four, 6:eleven, 8:thirteen]
fansh&gt; m.setList(["ten"]) |Str value-&gt;Int| { value.size }
[1:one, 2:two, 3:ten, 4:four, 6:eleven, 8:thirteen]
</pre>

</ul>
<p>
Finally, it's worth noting that maps can be decomposed into two lists by using
the <code>keys</code> and <code>vals</code> methods to get lists of the keys or values, respectively.
This means you can use some list methods along with a map, e.g. to find which
keys are associated with the same value:
</p>

<pre>
fansh&gt; bylength := ["one":3, "two":3, "three":5, "four":4, "five":4]
[four:4, one:3, two:3, three:5, five:4]
fansh&gt; bylength.keys.groupBy { bylength[it] }
[3:[one, two], 4:[four, five], 5:[three]]
</pre>

<div id="2024-02-26: Functional Programming in Fantom-Strings"><h2 id="Strings" class="header"><a href="#2024-02-26: Functional Programming in Fantom-Strings">Strings</a></h2></div>

<p>
As with the List, there are various kinds of operations supported on strings which
accept functions as one of their parameters. However, these are now reduced to a 
small "core set" of methods:
</p>

<table>
<thead>
<tr>
<th>
Kind of Operation
</th>
<th>
Kind of Result
</th>
<th>
Methods on Map
</th>
</tr>
</thead>
<tbody>
<tr>
<td>
collapse to value
</td>
<td>
return a single overall value from function results
</td>
<td>
<code>all</code> <code>any</code>
</td>
</tr>
<tr>
<td>
iteration
</td>
<td>
applies function to all, or sublist of, values
</td>
<td>
<code>each</code> <code>eachr</code>
</td>
</tr>
<tr>
<td>
search
</td>
<td>
return a new value or list of list values based on function results
</td>
<td>
<code>eachWhile</code>
</td>
</tr>
</tbody>
</table>

<p>
These methods all work analogously to those on lists, except that the function is given each (<code>Int</code>) character and its
index in turn:
</p>

<pre>
fansh&gt; "aBcDeF".all { isUpper }
false
fansh&gt; "aBcDeF".any { isUpper }
true
fansh&gt; "aBcDeF".each |Int char, Int index| { echo("$char ${char.toChar} at $index") }
97 a at 0
66 B at 1
99 c at 2
68 D at 3
101 e at 4
70 F at 5
</pre>

<p>
Strings can be converted to or from lists of chars, using <code>chars</code> and <code>Str.fromChars</code>, respectively.
These methods allow the use of useful list methods. For example, to keep only the upper case 
letters in a string:
</p>

<pre>
fansh&gt; Str.fromChars("aBcDeF123gH".chars.findAll { isUpper })
BDFH
</pre>

<div id="2024-02-26: Functional Programming in Fantom-Other Important Uses"><h2 id="Other Important Uses" class="header"><a href="#2024-02-26: Functional Programming in Fantom-Other Important Uses">Other Important Uses</a></h2></div>

<div id="2024-02-26: Functional Programming in Fantom-Other Important Uses-Files"><h3 id="Files" class="header"><a href="#2024-02-26: Functional Programming in Fantom-Other Important Uses-Files">Files</a></h3></div>

<p>
Functions are used throughout the standard library. A good example is in the
<code>File</code> library, where two methods provide a basic iteration structure with
functions filling out the details.
</p>

<ul>
<li>
<code>eachLine</code> - passes each line in the file, as a string, to the given function
<pre>
fansh&gt; `projects/pclTextBox/fan/Distance.fan`.toFile.eachLine { echo(it) }
** A collection of algorithms for measuring the distance between two strings.
**
** These distance measures are based on the number of changes needed to make
** the sequence of characters in one string match that in the other. Different
** algorithms use different operations.
**
class Distance
{
</pre>

</ul>
<p>
(OUTPUT TRUNCATED!)
</p>

<ul>
<li>
<code>walk</code> - if the file is a directory, then recursively walks through every file
  in the directory, passing each file to the function. If the file is a file, 
  then calls the function once only, with the file itself.
<pre>
fansh&gt; `projects/pclTextBox/fan/`.toFile.walk { echo(it) }
projects/pclTextBox/fan/
projects/pclTextBox/fan/Distance.fan
projects/pclTextBox/fan/Format.fan
projects/pclTextBox/fan/Ngrams.fan
projects/pclTextBox/fan/Phonetic.fan
projects/pclTextBox/fan/Stemmer.fan
projects/pclTextBox/fan/Stopwords.fan
</pre>

</ul>
<div id="2024-02-26: Functional Programming in Fantom-Other Important Uses-Range"><h3 id="Range" class="header"><a href="#2024-02-26: Functional Programming in Fantom-Other Important Uses-Range">Range</a></h3></div>

<p>
The range datatype supports <code>each</code>, <code>eachWhile</code> and <code>map</code>.
</p>

<pre>
fansh&gt; (10..15).each { echo(it) }
10
11
12
13
14
15
fansh&gt; (10..15).map { "Id: $it" }
[Id: 10, Id: 11, Id: 12, Id: 13, Id: 14, Id: 15]
</pre>

    </div>
    <hr>
    <small>Page from Peter's <a href="../index.html">Scrapbook</a>, output from a <a href="http://vimwiki.github.io/">VimWiki</a> on 2024-02-26.</small>
</body>
</html>
