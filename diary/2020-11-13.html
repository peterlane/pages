<html>
<head>
    <link rel="Stylesheet" type="text/css" href="../style.css" />
    <title>2020-11-13</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
</head>
<body>
    <div class="content">
    
<div id="2020-11-13: Text Classification with jRuby and Weka"><h1 id="2020-11-13: Text Classification with jRuby and Weka" class="header"><a href="#2020-11-13: Text Classification with jRuby and Weka">2020-11-13: Text Classification with jRuby and Weka</a></h1></div>

<p>
<a href="https://www.jruby.org">jRuby</a> is a perfect language to use with Java
libraries. Today I made a script to do some simple text classification using
<a href="http://www.cs.waikato.ac.nz/ml/weka/">Weka</a>.
</p>

<p>
For data, I use the BBC dataset from
<a href="https://www.kaggle.com/shivamkushwaha/bbc-full-text-document-classification">Kaggle</a>,
selecting two classes: entertainment and tech. 
</p>

<p>
So I have a parent folder, "bbc/", with two child folders, "entertainment" and
"tech". Each file in the child folders is a text document, and will form an
instance for the classifier to work on. The classes each have around 400
documents, and the total word count is 330,000.
</p>

<div id="2020-11-13: Text Classification with jRuby and Weka-Overall process"><h2 id="Overall process" class="header"><a href="#2020-11-13: Text Classification with jRuby and Weka-Overall process">Overall process</a></h2></div>

<p>
The overall process can be divided into several steps. (Here, I keep things
relatively simple, to investigate the API.)
</p>

<p>
First, the text must be converted into words only, so all numbers and
punctuation are removed, and all letters are converted to lowercase. So
"Address: 3 High Street, London." becomes the four words "address high street
london".
</p>

<p>
Second, it is useful to simplify the words themselves. Common words (like
"the", "a") are removed, using a stop-list. Also, stemming is applied so that
different words like "walks" and "walking" are reduced to their stem "walk";
stemming increases the number of words in different documents which match.
</p>

<p>
Third, the text files are converted into a representation where each text file
is an <em>instance</em> made from the <em>values</em> for a number of <em>attributes</em>, and a
<em>class</em> label. The class label has two values, one for "entertainment" and one
for "tech", depending on which folder the text file is in. Each attribute
represents one word, and has values 1 or 0 (whether the word for that attribute
is present in this instance or not).
</p>

<p>
Fourth, the number of attributes is usually very large at this stage, because
natural language uses a lot of words. An attibute selection technique reduces
the number of attributes to a manageable size.
</p>

<p>
Fifth, build some classification models. Three algorithms are used: Naive
Bayes, Decision Trees and Support Vector Machines. For each of these
algorithms, 10-fold cross validation is used to derive an overall accuracy.
</p>


<div id="2020-11-13: Text Classification with jRuby and Weka-Script"><h2 id="Script" class="header"><a href="#2020-11-13: Text Classification with jRuby and Weka-Script">Script</a></h2></div>

<p>
jRuby works well with Java libraries. Not only is it easy to import a library,
but names are transformed into Ruby formats. So a setter like
<code>filter.setOutputWordCounts(false)</code> can be written as
<code>filter.output_word_counts = false</code>, etc. 
</p>

<p>
The script follows. It is actually one file, but broken up with some explanations.
</p>

<p>
At the start of the script, load in the Weka library (I am using version 3.8.4)
and import the required classes, so each class can be referred to without an
import path. 
</p>

<pre>
<span style="color: #888888"># Aim to build a simple text classifier on BBC dataset</span>
<span style="color: #888888">#</span>
<span style="color: #003388">require</span> <span style="color: #dd2200;background-color: #fff0f0">"java"</span>
<span style="color: #003388">require_relative</span> <span style="color: #dd2200;background-color: #fff0f0">"weka.jar"</span>

<span style="">java_import</span> <span style="">[</span>
  <span style="color: #dd2200;background-color: #fff0f0">"weka.attributeSelection.CorrelationAttributeEval"</span><span style="">,</span>
  <span style="color: #dd2200;background-color: #fff0f0">"weka.attributeSelection.Ranker"</span><span style="">,</span>
  <span style="color: #dd2200;background-color: #fff0f0">"weka.classifiers.bayes.NaiveBayes"</span><span style="">,</span>
  <span style="color: #dd2200;background-color: #fff0f0">"weka.classifiers.functions.SMO"</span><span style="">,</span>
  <span style="color: #dd2200;background-color: #fff0f0">"weka.classifiers.trees.J48"</span><span style="">,</span>
  <span style="color: #dd2200;background-color: #fff0f0">"weka.core.converters.TextDirectoryLoader"</span><span style="">,</span>
  <span style="color: #dd2200;background-color: #fff0f0">"weka.core.stopwords.Rainbow"</span><span style="">,</span>
  <span style="color: #dd2200;background-color: #fff0f0">"weka.core.tokenizers.WordTokenizer"</span><span style="">,</span>
  <span style="color: #dd2200;background-color: #fff0f0">"weka.core.stemmers.LovinsStemmer"</span><span style="">,</span>
  <span style="color: #dd2200;background-color: #fff0f0">"weka.core.converters.ArffSaver"</span><span style="">,</span>
  <span style="color: #dd2200;background-color: #fff0f0">"weka.filters.supervised.attribute.AttributeSelection"</span><span style="">,</span>
  <span style="color: #dd2200;background-color: #fff0f0">"weka.filters.unsupervised.attribute.StringToWordVector"</span><span style="">,</span>
<span style="">]</span>
</pre>

<p>
The following method was created as our preprocess steps applies two of Weka's
filters. Weka requires us to input each instance in turn to the filter, tell
the filter the current batch has finished, and then retrieve the instances.
Notice that the output <code>result</code> may have a different structure (number and type
of attributes) to the input <code>instances</code>.
</p>

<pre>
<span style="color: #008800;font-weight: bold">def</span> <span style="color: #0066bb;font-weight: bold">apply_filter</span><span style="">(</span><span style="">instances</span><span style="">,</span> <span style="">filter</span><span style="">)</span>
  <span style="">filter</span><span style="">.</span><span style="color: #0066bb;font-weight: bold">setInputFormat</span><span style="">(</span><span style="">instances</span><span style="">)</span>
  <span style="">instances</span><span style="">.</span><span style="color: #0066bb;font-weight: bold">each</span> <span style="color: #008800;font-weight: bold">do</span> <span style="">|</span><span style="">instance</span><span style="">|</span>
    <span style="">filter</span><span style="">.</span><span style="color: #0066bb;font-weight: bold">input</span><span style="">(</span><span style="">instance</span><span style="">)</span>
  <span style="color: #008800;font-weight: bold">end</span>
  <span style="">filter</span><span style="">.</span><span style="color: #0066bb;font-weight: bold">batch_finished</span>

  <span style="">result</span> <span style="">=</span> <span style="">filter</span><span style="">.</span><span style="color: #0066bb;font-weight: bold">output_format</span>
  <span style="color: #008800">loop</span> <span style="color: #008800;font-weight: bold">do</span>
    <span style="">instance</span> <span style="">=</span> <span style="">filter</span><span style="">.</span><span style="color: #0066bb;font-weight: bold">output</span>
    <span style="color: #008800;font-weight: bold">break</span> <span style="color: #008800;font-weight: bold">if</span> <span style="">instance</span><span style="">.</span><span style="color: #0066bb;font-weight: bold">nil?</span>
    <span style="">result</span><span style="">.</span><span style="color: #0066bb;font-weight: bold">add</span><span style="">(</span><span style="">instance</span><span style="">)</span>
  <span style="color: #008800;font-weight: bold">end</span>

  <span style="">result</span>
<span style="color: #008800;font-weight: bold">end</span>
</pre>

<p>
The preprocess method covers the first four(!) steps described above. 
</p>

<p>
Weka provides <code>TextDirectoryLoader</code> to load the text documents from the two
folders. This process leaves each instance with two attributes: one is the text
of the document, and the second is its class label (the name of the child
folder). 
</p>

<p>
Step 1 is done using a Ruby regular expression, to replace all non-alphabetic
characters with spaces. 
</p>

<p>
Steps 2-3 are done using a <code>StringToWordVector</code> filter. In this filter, I set
the stemmer and stopwords handlers, tell it to convert the text to lower case
and tokenise the string as words (rather than character sequences). Setting
<code>output_word_counts</code> to false means the values will be 1 or 0, not actual word
counts.
</p>

<p>
Step 4 is achieved using a second filter, <code>CorrelationAttributeEval</code>, along
with a ranking algorithm to pick the most predictive 300 attributes.
</p>

<pre>
<span style="color: #008800;font-weight: bold">def</span> <span style="color: #0066bb;font-weight: bold">preprocess</span><span style="">(</span><span style="">text_dir</span><span style="">)</span>
  <span style="">loader</span> <span style="">=</span> <span style="color: #003366;font-weight: bold">TextDirectoryLoader</span><span style="">.</span><span style="color: #0066bb;font-weight: bold">new</span>
  <span style="">loader</span><span style="">.</span><span style="color: #0066bb;font-weight: bold">source</span> <span style="">=</span> <span style="">java</span><span style="">::</span><span style="">io</span><span style="">::</span><span style="color: #003366;font-weight: bold">File</span><span style="">.</span><span style="color: #0066bb;font-weight: bold">new</span><span style="">(</span><span style="">text_dir</span><span style="">)</span>
  <span style="">instances</span> <span style="">=</span> <span style="">loader</span><span style="">.</span><span style="color: #0066bb;font-weight: bold">data_set</span>

  <span style="color: #888888"># remove numbers/punctuation - step 1</span>
  <span style="">instances</span><span style="">.</span><span style="color: #0066bb;font-weight: bold">each</span> <span style="color: #008800;font-weight: bold">do</span> <span style="">|</span><span style="">instance</span><span style="">|</span>
    <span style="">text</span> <span style="">=</span> <span style="">instance</span><span style="">.</span><span style="color: #0066bb;font-weight: bold">string_value</span><span style="">(</span><span style="color: #0000dd;font-weight: bold">0</span><span style="">)</span> <span style="color: #888888"># the text is in the first attribute</span>
    <span style="">text</span><span style="">.</span><span style="color: #0066bb;font-weight: bold">gsub!</span><span style="">(</span><span style="color: #008800">/[^a-zA-Z]/</span><span style="">,</span> <span style="color: #dd2200;background-color: #fff0f0">' '</span><span style="">)</span>
    <span style="">instance</span><span style="">.</span><span style="color: #0066bb;font-weight: bold">set_value</span><span style="">(</span><span style="color: #0000dd;font-weight: bold">0</span><span style="">,</span> <span style="">text</span><span style="">)</span>
  <span style="color: #008800;font-weight: bold">end</span>

  <span style="color: #888888"># turn into vector of words, applying filters - steps 2 &amp; 3</span>
  <span style="">filter</span> <span style="">=</span> <span style="color: #003366;font-weight: bold">StringToWordVector</span><span style="">.</span><span style="color: #0066bb;font-weight: bold">new</span>
  <span style="">filter</span><span style="">.</span><span style="color: #0066bb;font-weight: bold">lower_case_tokens</span> <span style="">=</span> <span style="color: #008800">true</span>
  <span style="">filter</span><span style="">.</span><span style="color: #0066bb;font-weight: bold">output_word_counts</span> <span style="">=</span> <span style="color: #008800">false</span>
  <span style="">filter</span><span style="">.</span><span style="color: #0066bb;font-weight: bold">stemmer</span> <span style="">=</span> <span style="color: #003366;font-weight: bold">LovinsStemmer</span><span style="">.</span><span style="color: #0066bb;font-weight: bold">new</span>
  <span style="">filter</span><span style="">.</span><span style="color: #0066bb;font-weight: bold">stopwords_handler</span> <span style="">=</span> <span style="color: #003366;font-weight: bold">Rainbow</span><span style="">.</span><span style="color: #0066bb;font-weight: bold">new</span>
  <span style="">filter</span><span style="">.</span><span style="color: #0066bb;font-weight: bold">tokenizer</span> <span style="">=</span> <span style="color: #003366;font-weight: bold">WordTokenizer</span><span style="">.</span><span style="color: #0066bb;font-weight: bold">new</span>
  <span style="color: #888888"># -- apply the filter</span>
  <span style="">instances</span> <span style="">=</span> <span style="">apply_filter</span><span style="">(</span><span style="">instances</span><span style="">,</span> <span style="">filter</span><span style="">)</span>
  <span style="color: #888888"># identify the class label</span>
  <span style="">instances</span><span style="">.</span><span style="color: #0066bb;font-weight: bold">class_index</span> <span style="">=</span> <span style="color: #0000dd;font-weight: bold">0</span>

  <span style="color: #888888"># reduce number of attributes to 300 - step 4</span>
  <span style="">selector</span> <span style="">=</span> <span style="color: #003366;font-weight: bold">AttributeSelection</span><span style="">.</span><span style="color: #0066bb;font-weight: bold">new</span>
  <span style="">selector</span><span style="">.</span><span style="color: #0066bb;font-weight: bold">evaluator</span> <span style="">=</span> <span style="color: #003366;font-weight: bold">CorrelationAttributeEval</span><span style="">.</span><span style="color: #0066bb;font-weight: bold">new</span>
  <span style="">ranker</span> <span style="">=</span> <span style="color: #003366;font-weight: bold">Ranker</span><span style="">.</span><span style="color: #0066bb;font-weight: bold">new</span>
  <span style="">ranker</span><span style="">.</span><span style="color: #0066bb;font-weight: bold">num_to_select</span> <span style="">=</span> <span style="color: #0000dd;font-weight: bold">300</span>
  <span style="">selector</span><span style="">.</span><span style="color: #0066bb;font-weight: bold">search</span> <span style="">=</span> <span style="">ranker</span>
  <span style="color: #888888"># -- apply the filter</span>
  <span style="">instances</span> <span style="">=</span> <span style="">apply_filter</span><span style="">(</span><span style="">instances</span><span style="">,</span> <span style="">selector</span><span style="">)</span>

  <span style="">instances</span>
<span style="color: #008800;font-weight: bold">end</span>
</pre>

<p>
Step 5 is the task of the <code>evaluate_classifier</code> method, used to test a given
classification algorithm. Weka provides methods on instances to access
train/test sets for k-fold cross-validation, so we use those to build and
evaluate a classifier for each fold. Notice the use of ruby's <code>const_get</code> to
access the classifier's class from its string name. Most of this method
collates and reports the required numbers for evaluation:
</p>

<pre>
<span style="color: #008800;font-weight: bold">def</span> <span style="color: #0066bb;font-weight: bold">evaluate_classifier</span><span style="">(</span><span style="">classifier</span><span style="">,</span> <span style="">instances</span><span style="">,</span> <span style="">k</span><span style="">=</span><span style="color: #0000dd;font-weight: bold">10</span><span style="">)</span>
  <span style="">true_pos</span> <span style="">=</span> <span style="color: #0000dd;font-weight: bold">0</span>
  <span style="">true_neg</span> <span style="">=</span> <span style="color: #0000dd;font-weight: bold">0</span>
  <span style="">false_pos</span> <span style="">=</span> <span style="color: #0000dd;font-weight: bold">0</span>
  <span style="">false_neg</span> <span style="">=</span> <span style="color: #0000dd;font-weight: bold">0</span>

  <span style="">k</span><span style="">.</span><span style="color: #0066bb;font-weight: bold">times</span> <span style="color: #008800;font-weight: bold">do</span> <span style="">|</span><span style="">i</span><span style="">|</span>
    <span style="">model</span> <span style="">=</span> <span style="color: #003366;font-weight: bold">Object</span><span style="">.</span><span style="color: #0066bb;font-weight: bold">const_get</span><span style="">(</span><span style="">classifier</span><span style="">).</span><span style="color: #0066bb;font-weight: bold">new</span>
    <span style="">train</span> <span style="">=</span> <span style="">instances</span><span style="">.</span><span style="color: #0066bb;font-weight: bold">train_cv</span><span style="">(</span><span style="">k</span><span style="">,</span> <span style="">i</span><span style="">)</span>
    <span style="color: #003388">test</span> <span style="">=</span> <span style="">instances</span><span style="">.</span><span style="color: #0066bb;font-weight: bold">test_cv</span><span style="">(</span><span style="">k</span><span style="">,</span> <span style="">i</span><span style="">)</span>
    <span style="">model</span><span style="">.</span><span style="color: #0066bb;font-weight: bold">build_classifier</span><span style="">(</span><span style="">train</span><span style="">)</span>

    <span style="color: #003388">test</span><span style="">.</span><span style="color: #0066bb;font-weight: bold">each</span> <span style="color: #008800;font-weight: bold">do</span> <span style="">|</span><span style="">instance</span><span style="">|</span>
      <span style="">result</span> <span style="">=</span> <span style="">model</span><span style="">.</span><span style="color: #0066bb;font-weight: bold">classify_instance</span><span style="">(</span><span style="">instance</span><span style="">)</span>
      <span style="color: #008800;font-weight: bold">if</span> <span style="">instance</span><span style="">.</span><span style="color: #0066bb;font-weight: bold">class_value</span> <span style="">&gt;</span> <span style="color: #0000dd;font-weight: bold">0.5</span> <span style="color: #888888"># positive group</span>
        <span style="color: #008800;font-weight: bold">if</span> <span style="">(</span><span style="">instance</span><span style="">.</span><span style="color: #0066bb;font-weight: bold">class_value</span><span style="">-</span><span style="">result</span><span style="">).</span><span style="color: #0066bb;font-weight: bold">abs</span> <span style="">&lt;</span> <span style="color: #0000dd;font-weight: bold">0.5</span>
          <span style="">true_pos</span> <span style="">+=</span> <span style="color: #0000dd;font-weight: bold">1</span>
        <span style="color: #008800;font-weight: bold">else</span>
          <span style="">false_neg</span> <span style="">+=</span> <span style="color: #0000dd;font-weight: bold">1</span>
        <span style="color: #008800;font-weight: bold">end</span>
      <span style="color: #008800;font-weight: bold">else</span> <span style="color: #888888"># negative group</span>
        <span style="color: #008800;font-weight: bold">if</span> <span style="">(</span><span style="">instance</span><span style="">.</span><span style="color: #0066bb;font-weight: bold">class_value</span><span style="">-</span><span style="">result</span><span style="">).</span><span style="color: #0066bb;font-weight: bold">abs</span> <span style="">&lt;</span> <span style="color: #0000dd;font-weight: bold">0.5</span>
          <span style="">true_neg</span> <span style="">+=</span> <span style="color: #0000dd;font-weight: bold">1</span>
        <span style="color: #008800;font-weight: bold">else</span>
          <span style="">false_pos</span> <span style="">+=</span> <span style="color: #0000dd;font-weight: bold">1</span>
        <span style="color: #008800;font-weight: bold">end</span>
      <span style="color: #008800;font-weight: bold">end</span>
    <span style="color: #008800;font-weight: bold">end</span>
  <span style="color: #008800;font-weight: bold">end</span>
  <span style="">true_pos</span> <span style="">/=</span> <span style="">k</span><span style="">.</span><span style="color: #0066bb;font-weight: bold">to_f</span>
  <span style="">true_neg</span> <span style="">/=</span> <span style="">k</span><span style="">.</span><span style="color: #0066bb;font-weight: bold">to_f</span>
  <span style="">false_pos</span> <span style="">/=</span> <span style="">k</span><span style="">.</span><span style="color: #0066bb;font-weight: bold">to_f</span>
  <span style="">false_neg</span> <span style="">/=</span> <span style="">k</span><span style="">.</span><span style="color: #0066bb;font-weight: bold">to_f</span>
  <span style="">precision</span> <span style="">=</span> <span style="">true_pos</span><span style="">/</span><span style="">(</span><span style="">true_pos</span><span style="">+</span><span style="">false_neg</span><span style="">)</span>
  <span style="">recall</span> <span style="">=</span> <span style="">true_neg</span><span style="">/</span><span style="">(</span><span style="">true_neg</span><span style="">+</span><span style="">false_pos</span><span style="">)</span>
  <span style="">geometric_mean</span> <span style="">=</span> <span style="color: #003366;font-weight: bold">Math</span><span style="">.</span><span style="color: #0066bb;font-weight: bold">sqrt</span><span style="">(</span><span style="">precision</span><span style="">*</span><span style="">recall</span><span style="">)</span>

  <span style="color: #003388">puts</span> <span style="color: #dd2200;background-color: #fff0f0">"Classifier: </span><span style="color: #3333bb;background-color: #fff0f0">#{</span><span style="">classifier</span><span style="color: #3333bb;background-color: #fff0f0">}</span><span style="color: #dd2200;background-color: #fff0f0"> "</span>
  <span style="color: #003388">puts</span> <span style="color: #dd2200;background-color: #fff0f0">" -- Precision      </span><span style="color: #3333bb;background-color: #fff0f0">#{</span><span style="">precision</span><span style="color: #3333bb;background-color: #fff0f0">}</span><span style="color: #dd2200;background-color: #fff0f0">"</span>
  <span style="color: #003388">puts</span> <span style="color: #dd2200;background-color: #fff0f0">" -- Recall         </span><span style="color: #3333bb;background-color: #fff0f0">#{</span><span style="">recall</span><span style="color: #3333bb;background-color: #fff0f0">}</span><span style="color: #dd2200;background-color: #fff0f0">"</span>
  <span style="color: #003388">puts</span> <span style="color: #dd2200;background-color: #fff0f0">" -- Geometric mean </span><span style="color: #3333bb;background-color: #fff0f0">#{</span><span style="">geometric_mean</span><span style="color: #3333bb;background-color: #fff0f0">}</span><span style="color: #dd2200;background-color: #fff0f0">"</span>
  <span style="color: #003388">puts</span>
<span style="color: #008800;font-weight: bold">end</span>
</pre>

<p>
The final step is to create some actual data and build some classification
models. Notice how the Weka class names are passed to <code>evaluate_classifier</code>,
which are used to create the right classifier.
</p>

<pre>
<span style="color: #008800;font-weight: bold">def</span> <span style="color: #0066bb;font-weight: bold">run_expt</span>
  <span style="">data</span> <span style="">=</span> <span style="">preprocess</span><span style="">(</span><span style="color: #dd2200;background-color: #fff0f0">'bbc/'</span><span style="">)</span>

  <span style="">evaluate_classifier</span><span style="">(</span><span style="color: #dd2200;background-color: #fff0f0">'J48'</span><span style="">,</span> <span style="">data</span><span style="">)</span>
  <span style="">evaluate_classifier</span><span style="">(</span><span style="color: #dd2200;background-color: #fff0f0">'NaiveBayes'</span><span style="">,</span> <span style="">data</span><span style="">)</span>
  <span style="">evaluate_classifier</span><span style="">(</span><span style="color: #dd2200;background-color: #fff0f0">'SMO'</span><span style="">,</span> <span style="">data</span><span style="">)</span>
<span style="color: #008800;font-weight: bold">end</span>

<span style="">run_expt</span>
</pre>

<p>
On my system, the script runs through in about 20 seconds. The output is:
</p>

<pre>
Classifier: J48 
 -- Precision      0.9201995012468828
 -- Recall         0.9792746113989638
 -- Geometric mean 0.9492776248248251

Classifier: NaiveBayes 
 -- Precision      0.9825436408977556
 -- Recall         0.9715025906735751
 -- Geometric mean 0.9770075192044412

Classifier: SMO 
 -- Precision      0.9750623441396509
 -- Recall         0.9896373056994819
 -- Geometric mean 0.9823227937614932
</pre>

    </div>
    <hr>
    <small>Page from Peter's <a href="../index.html">Scrapbook</a>, output from a <a href="http://vimwiki.github.io/">VimWiki</a> on 2024-01-29.</small>
</body>
</html>
