<html>
<head>
    <link rel="Stylesheet" type="text/css" href="../style.css" />
    <title>2022-04-08</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
</head>
<body>
    <div class="content">
    
<div id="2022-04-08: Reading CSV Files"><h1 id="2022-04-08: Reading CSV Files" class="header"><a href="#2022-04-08: Reading CSV Files">2022-04-08: Reading CSV Files</a></h1></div>

<p>
Reading a CSV file of data is a common task for machine-learning applications.
This note shows how to read a file of CSV data, using the Apache Commons CSV
library, and convert it into a list of instances, where each instance is an
appropriate Java record.
</p>

<div id="2022-04-08: Reading CSV Files-Example: Iris Dataset"><h2 id="Example: Iris Dataset" class="header"><a href="#2022-04-08: Reading CSV Files-Example: Iris Dataset">Example: Iris Dataset</a></h2></div>

<p>
The example used here is the Iris Data set from the UCI database: 
<a href="https://archive.ics.uci.edu/dataset/53/iris">download</a> iris.data.
</p>

<p>
This dataset has four attributes and a label, stored within a CSV file. The first
few records are:
</p>

<pre>
5.1,3.5,1.4,0.2,Iris-setosa
4.9,3.0,1.4,0.2,Iris-setosa
4.7,3.2,1.3,0.2,Iris-setosa
4.6,3.1,1.5,0.2,Iris-setosa
...
</pre>

<p>
The four attributes are stored as double values in a Java record, and the label
as a string:
</p>

<pre>
<span style="">record</span> <span style="color: #0066bb;font-weight: bold">IrisInstance</span><span style="">(</span>
    <span style="color: #888888;font-weight: bold">double</span> <span style="">sepalLength</span><span style="">,</span> 
    <span style="color: #888888;font-weight: bold">double</span> <span style="">sepalWidth</span><span style="">,</span>
    <span style="color: #888888;font-weight: bold">double</span> <span style="">petalLength</span><span style="">,</span>
    <span style="color: #888888;font-weight: bold">double</span> <span style="">petalWidth</span><span style="">,</span>
    <span style="color: #bb0066;font-weight: bold">String</span> <span style="">label</span>
  <span style="">)</span> <span style="">{}</span>
</pre>

<div id="2022-04-08: Reading CSV Files-Apache Commons CSV"><h2 id="Apache Commons CSV" class="header"><a href="#2022-04-08: Reading CSV Files-Apache Commons CSV">Apache Commons CSV</a></h2></div>

<p>
The <a href="https://commons.apache.org/proper/commons-csv/">Apache Commons CSV</a> library
provides functionality to handle CSV files - here, we focus on reading a CSV
file. 
</p>

<p>
The <code>CSVFormat</code> class provides a list of CSV readers, as CSV is not a
standardised format. For straightforward files, we can use the
<a href="https://datatracker.ietf.org/doc/html/rfc4180">RFC4180</a> definition via
<code>CSVFormat.RFC4180</code>. This provides access to a <code>parse</code> method, which reads from
an input stream and produces an iterable instance of <code>CSVParser</code>.
</p>

<p>
It is convenient to process the parsed output as a stream:
</p>

<pre>
    <span style="color: #008800;font-weight: bold">try</span> <span style="">(</span><span style="color: #bb0066;font-weight: bold">Reader</span> <span style="">in</span> <span style="">=</span> <span style="color: #bb0066;font-weight: bold">Files</span><span style="">.</span><span style="color: #336699">newBufferedReader</span> <span style="">(</span><span style="">file</span><span style="">))</span> <span style="">{</span>                <span style="color: #888888">// &lt;1&gt;</span>
      <span style="color: #bb0066;font-weight: bold">List</span><span style="">&lt;</span><span style="color: #bb0066;font-weight: bold">IrisInstance</span><span style="">&gt;</span> <span style="">data</span> <span style="">=</span> <span style="color: #bb0066;font-weight: bold">CSVFormat</span><span style="">.</span><span style="color: #336699">RFC4180</span><span style="">.</span><span style="color: #336699">parse</span><span style="">(</span><span style="">in</span><span style="">).</span><span style="color: #336699">stream</span><span style="">()</span>  <span style="color: #888888">// &lt;2&gt;</span>
        <span style="">.</span><span style="color: #336699">map</span><span style="">(</span><span style="color: #336699">IrisInstance:</span><span style="">:</span><span style="">fromCSVRecord</span><span style="">)</span>                             <span style="color: #888888">// &lt;3&gt;</span>
        <span style="">.</span><span style="color: #336699">flatMap</span><span style="">(</span><span style="color: #336699">Optional:</span><span style="">:</span><span style="">stream</span><span style="">)</span>                                    <span style="color: #888888">// &lt;4&gt;</span>
        <span style="">.</span><span style="color: #336699">toList</span><span style="">();</span>                                                    <span style="color: #888888">// &lt;5&gt;</span>
</pre>

<ol>
<li>
Opens an input reader using <code>try-with-resources</code>, to ensure it is safely closed.

<li>
Parses the input reader and creates a Java stream of <code>CSVRecord</code> instances.

<li>
Converts each <code>CSVRecord</code> into an optional <code>IrisData</code>.

<li>
Removes the empty optional items.

<li>
Converts the stream to a list.

</ol>
<div id="2022-04-08: Reading CSV Files-Converting CSVRecord to IrisInstance"><h2 id="Converting CSVRecord to IrisInstance" class="header"><a href="#2022-04-08: Reading CSV Files-Converting CSVRecord to IrisInstance">Converting CSVRecord to IrisInstance</a></h2></div>

<p>
The library represents each line of data from the CSV file as an instance of its
<code>CSVRecord</code> class: this must be converted into an <code>IrisInstance</code> record, to 
properly represent the data.
</p>

<p>
The conversion should ensure there are enough fields in the line, and also
convert the first four attributes into doubles, checking that each attribute is
correctly formatted.
</p>

<p>
As it is possible for a <code>CSVRecord</code> not to be a valid <code>IrisInstance</code>, the conversion
uses <code>Optional</code>, with an empty optional used when the conversion could not be made:
as shown above, these are then filtered out.
</p>

<p>
The conversion method is added to the <code>IrisInstance</code> record as a static method:
</p>

<pre>
  <span style="color: #008800;font-weight: bold">public</span> <span style="color: #008800;font-weight: bold">static</span> <span style="color: #bb0066;font-weight: bold">Optional</span><span style="">&lt;</span><span style="color: #bb0066;font-weight: bold">IrisInstance</span><span style="">&gt;</span> <span style="color: #0066bb;font-weight: bold">fromCSVRecord</span> <span style="">(</span><span style="color: #bb0066;font-weight: bold">CSVRecord</span> <span style="">record</span><span style="">)</span> <span style="">{</span>
    <span style="color: #008800;font-weight: bold">if</span> <span style="">(</span><span style="">record</span><span style="">.</span><span style="color: #336699">size</span><span style="">()</span> <span style="">==</span> <span style="color: #0000dd;font-weight: bold">5</span><span style="">)</span> <span style="">{</span> <span style="color: #888888">// ensure we have enough parts    // &lt;1&gt;</span>
      <span style="color: #008800;font-weight: bold">try</span> <span style="">{</span>
        <span style="color: #888888;font-weight: bold">double</span> <span style="">sepalLength</span> <span style="">=</span> <span style="color: #bb0066;font-weight: bold">Double</span><span style="">.</span><span style="color: #336699">parseDouble</span><span style="">(</span><span style="">record</span><span style="">.</span><span style="color: #336699">get</span><span style="">(</span><span style="color: #0000dd;font-weight: bold">0</span><span style="">));</span> <span style="color: #888888">// &lt;2&gt;</span>
        <span style="color: #888888;font-weight: bold">double</span> <span style="">sepalWidth</span> <span style="">=</span> <span style="color: #bb0066;font-weight: bold">Double</span><span style="">.</span><span style="color: #336699">parseDouble</span><span style="">(</span><span style="">record</span><span style="">.</span><span style="color: #336699">get</span><span style="">(</span><span style="color: #0000dd;font-weight: bold">1</span><span style="">));</span>
        <span style="color: #888888;font-weight: bold">double</span> <span style="">petalLength</span> <span style="">=</span> <span style="color: #bb0066;font-weight: bold">Double</span><span style="">.</span><span style="color: #336699">parseDouble</span><span style="">(</span><span style="">record</span><span style="">.</span><span style="color: #336699">get</span><span style="">(</span><span style="color: #0000dd;font-weight: bold">2</span><span style="">));</span>
        <span style="color: #888888;font-weight: bold">double</span> <span style="">petalWidth</span> <span style="">=</span> <span style="color: #bb0066;font-weight: bold">Double</span><span style="">.</span><span style="color: #336699">parseDouble</span><span style="">(</span><span style="">record</span><span style="">.</span><span style="color: #336699">get</span><span style="">(</span><span style="color: #0000dd;font-weight: bold">3</span><span style="">));</span>

        <span style="color: #008800;font-weight: bold">return</span> <span style="color: #bb0066;font-weight: bold">Optional</span><span style="">.</span><span style="color: #336699">of</span><span style="">(</span><span style="color: #008800;font-weight: bold">new</span> <span style="color: #bb0066;font-weight: bold">IrisInstance</span><span style="">(</span><span style="">sepalLength</span><span style="">,</span>        <span style="color: #888888">// &lt;3&gt;</span>
              <span style="">sepalWidth</span><span style="">,</span> <span style="">petalLength</span><span style="">,</span> <span style="">petalWidth</span><span style="">,</span> <span style="">record</span><span style="">.</span><span style="color: #336699">get</span><span style="">(</span><span style="color: #0000dd;font-weight: bold">4</span><span style="">)));</span>

      <span style="">}</span> <span style="color: #008800;font-weight: bold">catch</span> <span style="">(</span><span style="color: #bb0066;font-weight: bold">NumberFormatException</span> <span style="">e</span><span style="">)</span> <span style="">{</span>
        <span style="">;</span>                                                       <span style="color: #888888">// &lt;4&gt;</span>
      <span style="">}</span>
    <span style="">}</span>

    <span style="color: #008800;font-weight: bold">return</span> <span style="color: #bb0066;font-weight: bold">Optional</span><span style="">.</span><span style="color: #336699">empty</span><span style="">();</span>                                    <span style="color: #888888">// &lt;5&gt;</span>
  <span style="">}</span>
</pre>

<ol>
<li>
Check that the record has enough fields (4 attributes + 1 label).

<li>
Parse each of the attributes out into a <code>Double</code>.

<li>
Assuming no errors, return an optional of <code>IrisInstance</code> with the appropriate values.

<li>
If there is a number format error, fall through (or report the error).

<li>
Something went wrong, so return an empty optional.

</ol>

    </div>
    <hr>
    <small>Page from Peter's <a href="../index.html">Scrapbook</a>, output from a <a href="http://vimwiki.github.io/">VimWiki</a> on 2024-01-29.</small>
</body>
</html>
