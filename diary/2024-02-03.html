<html>
<head>
    <link rel="Stylesheet" type="text/css" href="../style.css" />
    <title>2024-02-03</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
</head>
<body>
    <div class="content">
    
<div id="2024-02-03: Fantom&lt;-&gt;Java Interop for Iris Data Analysis"><h1 id="2024-02-03: Fantom&lt;-&gt;Java Interop for Iris Data Analysis" class="header"><a href="#2024-02-03: Fantom&lt;-&gt;Java Interop for Iris Data Analysis">2024-02-03: Fantom&lt;-&gt;Java Interop for Iris Data Analysis</a></h1></div>

<p>
As I've done in a few other notes, I will explore some simple data analysis
tools applied to the <a href="https://archive.ics.uci.edu/dataset/53/iris">iris</a>
dataset, but this time using Fantom. This will illustrate how to interact with
a Java library from Fantom for simple purposes.
</p>

<p>
The Java library used here is 
<a href="https://commons.apache.org/proper/commons-math/">Apache Commons Math</a>,
version 3.6.1.  Download
<a href="https://dlcdn.apache.org//commons/math/binaries/commons-math3-3.6.1-bin.zip">commons-math3-3.6.1.jar</a>
and place in "FANTOM/lib/java/ext", ready for Fantom to find and use.
</p>

<div id="2024-02-03: Fantom&lt;-&gt;Java Interop for Iris Data Analysis-Loading a CSV File"><h2 id="Loading a CSV File" class="header"><a href="#2024-02-03: Fantom&lt;-&gt;Java Interop for Iris Data Analysis-Loading a CSV File">Loading a CSV File</a></h2></div>

<p>
The iris dataset, "iris.data", is a CSV formatted dataset:
</p>

<pre>
5.1,3.5,1.4,0.2,Iris-setosa
4.9,3.0,1.4,0.2,Iris-setosa
4.7,3.2,1.3,0.2,Iris-setosa
4.6,3.1,1.5,0.2,Iris-setosa
...
</pre>

<p>
There are four numeric fields, followed by a class name. 
</p>

<p>
The first part of the program must load the dataset, creating a list of
<code>IrisInstance</code> instances, where each <code>IrisInstance</code> holds the data from one row
of the file.
</p>

<p>
The <code>IrisInstance</code> class is:
</p>

<pre>
class IrisInstance : Clusterable
{
  Float sepalLength
  Float sepalWidth
  Float petalLength
  Float petalWidth
  Str label

  new make(Str[] row)
  {
    this.sepalLength = row[0].toFloat
    this.sepalWidth = row[1].toFloat
    this.petalLength = row[2].toFloat
    this.petalWidth = row[3].toFloat
    this.label = row[4]
  }
}
</pre>

<p>
The constructor accepts a list of strings, and converts the first 4 values into 
Floats.
</p>

<p>
Fantom supports CSV file handling in its standard library: the "util" library
has a <a href="https://fantom.org/doc/util/CsvInStream">CsvInStream</a> class for reading
CSV files. With this, the code to read in the CSV file line-by-line and convert
into <code>IrisInstance</code> instances is:
</p>

<pre>
  static Void main(Str[] args) 
  {
    // step 1: read in CSV file
    echo("File name " + args[0])                                    // <1>
    data := IrisInstance[,]                                         // <2>

    util::CsvInStream(args[0].toUri.toFile.in).eachRow |Str[] row|  // <3>
    {
      if (row.size == 5)                                            // <4>
      {
        data.add(IrisInstance(row))
      }
    }
    echo("Read " + data.size + " instances")                        // <5>
  }
</pre>
<ol>
<li>
Get the filename from the input args.

<li>
Create an empty list to store the <code>IrisInstance</code> instances.

<li>
Use <code>CsvInStream</code> to read in each row of the given file.

<li>
If the row has enough fields, convert the row into an <code>IrisInstance</code> 
   instance and add it to the data list.

<li>
Finally, check the data was read OK by printing out the number of instances.

</ol>
<p>
Output:
</p>

<pre>
&gt; fan IrisData.fan iris.data
File name iris.data
Read 150 instances
</pre>

<div id="2024-02-03: Fantom&lt;-&gt;Java Interop for Iris Data Analysis-Descriptive Statistics"><h2 id="Descriptive Statistics" class="header"><a href="#2024-02-03: Fantom&lt;-&gt;Java Interop for Iris Data Analysis-Descriptive Statistics">Descriptive Statistics</a></h2></div>

<p>
The <a href="https://commons.apache.org/proper/commons-math/">Apache Commons Math</a>
library provides the class <code>DescriptiveStatistics</code>, which stores a collection
of values and provides methods to access statistical properties, such as
arithmetic mean, standard deviation, etc.
</p>

<p>
We can collect the values for a given attribute and return 
an instance of <code>DescriptiveStatistics</code> using the following method, which 
accepts an accessor function to retrieve the required attribute value:
</p>

<pre>
  static DescriptiveStatistics getStatistics(IrisInstance[] data, 
                                             |IrisInstance->Float| accessor)
  {
    ds := DescriptiveStatistics()               // <1>
    data.each { ds.addValue(accessor(it)) }     // <2>
    return ds
  }
</pre>
<ol>
<li>
Creates an instance of the required class from the library.

<li>
Uses an it-block to apply the accessor to every item in the data set.

</ol>
<p>
It is then straightforward to print out information for an attribute from the 
instance of <code>DescriptiveStatistics</code>:
</p>

<pre>
  static Void displayStatistics(Str name, DescriptiveStatistics statistics)
  {
    echo(name)
    echo(" -- Minimum: " + statistics.getMin.toLocale("0.00"))    // <1>
    echo(" -- Maximum: " + statistics.getMax.toLocale("0.00"))
    echo(" -- Mean:    " + statistics.getMean.toLocale("0.00"))
    echo(" -- Stddev:  " + statistics.getStandardDeviation.toLocale("0.00"))
  }
</pre>
<ol>
<li>
<code>toLocale</code> lets us specify how to represent the printed numbers.

</ol>
<p>
Finally, we call these methods using closures to specify each attribute value:
</p>

<pre>
    displayStatistics("Sepal Length", 
                      getStatistics(data) { it.sepalLength })
    displayStatistics("Sepal Width", 
                      getStatistics(data) { it.sepalWidth })
    displayStatistics("Petal Length", 
                      getStatistics(data) { it.petalLength })
    displayStatistics("Petal Width", 
                      getStatistics(data) { it.petalWidth })
</pre>

<p>
This produces the following output:
</p>

<pre>
Sepal Length
 -- Minimum: 4.30
 -- Maximum: 7.90
 -- Mean:    5.84
 -- Stddev:  0.83
Sepal Width
 -- Minimum: 2.00
 -- Maximum: 4.40
 -- Mean:    3.05
 -- Stddev:  0.43
Petal Length
 -- Minimum: 1.00
 -- Maximum: 6.90
 -- Mean:    3.76
 -- Stddev:  1.76
Petal Width
 -- Minimum: 0.10
 -- Maximum: 2.50
 -- Mean:    1.20
 -- Stddev:  0.76
</pre>

<div id="2024-02-03: Fantom&lt;-&gt;Java Interop for Iris Data Analysis-Clustering: Fantom&lt;-&gt;Java Interop"><h2 id="Clustering: Fantom&lt;-&gt;Java Interop" class="header"><a href="#2024-02-03: Fantom&lt;-&gt;Java Interop for Iris Data Analysis-Clustering: Fantom&lt;-&gt;Java Interop">Clustering: Fantom&lt;-&gt;Java Interop</a></h2></div>

<p>
We now want to cluster the Iris instances.  The
<a href="https://commons.apache.org/proper/commons-math/">Apache Commons Math</a> library
provides the class <code>KMeansPlusPlusClusterer</code>. Properties of the model are
set on constructing an instance of this class, including:
</p>

<ul>
<li>
expected number of clusters

<li>
maximum number of iterations

<li>
strategy for dealing with empty clusters

<li>
distance measure

</ul>
<p>
As the Iris dataset has three class labels, this example uses 3 as the expected
number of clusters, leaving the other properties to their default values.
</p>

<p>
The model is built by calling the <code>cluster</code> method with a collection of
<code>Clusterable</code> instances, and returns a list of <code>CentroidCluster</code> instances.
Each centroid cluster has a "centre", and the list of points assigned to that
cluster.
</p>

<p>
So we need to supply our "data" list to the <code>cluster</code> method as a collection 
of <code>Clusterable</code> instances. This requires first implementing the <code>Clusterable</code>
interface on the <code>IrisInstance</code> class, and then passing the list as a collection
to the <code>cluster</code> method. 
</p>

<p>
First, we need to make <code>IrisInstance</code> implement the <code>Clusterable</code> interface.
This requires a <code>getPoint</code> method to return an instance of Java's <code>double[]</code> - 
this type is mapped to <code>DoubleArray</code> by Fantom's interop library.
</p>

<pre>
class IrisInstance : Clusterable
{
  // ... same as above

  override DoubleArray? getPoint() {
    result := DoubleArray(4)
    
    result[0] = sepalLength
    result[1] = sepalWidth
    result[2] = petalLength
    result[3] = petalWidth

    return result
  }
}
</pre>

<p>
Second, we need to do some conversions on Fantom's <code>List</code> type, to and from
Java.  Fantom already has the ability to automatically convert <code>List</code> to and
from an array, so we take advantage of Java's
<a href="https://docs.oracle.com/en/java/javase/21/docs/api/java.base/java/util/Arrays.html">Arrays#asList</a>
method to convert the array into a collection. 
</p>

<p>
The return value is a Java <code>List</code>, and we can convert that back into an array
using Java's
<a href="https://docs.oracle.com/en/java/javase/21/docs/api/java.base/java/util/Collection.html">Collection#toArray</a>
method.
</p>

<pre>
    model := KMeansPlusPlusClusterer(3)
    model.cluster(Arrays.asList(data)).toArray.each |CentroidCluster cluster|
    {
      echo("Centre: " + Arrays.toString(cluster.getCenter.getPoint))  // <1>
      echo("Cluster has: " + cluster.getPoints.size + " points")
    }
</pre>
<ol>
<li>
<code>Arrays.toString</code> is another useful Java method, formatting the array into a 
   printable format.

</ol>
<p>
Output:
</p>

<pre>
Centre: [5.901612903225807, 2.748387096774194, 4.393548387096775, 1.4338709677419357]
Cluster has: 62 points
Centre: [6.8500000000000005, 3.073684210526315, 5.742105263157893, 2.0710526315789473]
Cluster has: 38 points
Centre: [5.005999999999999, 3.4180000000000006, 1.464, 0.2439999999999999]
Cluster has: 50 points
</pre>

<div id="2024-02-03: Fantom&lt;-&gt;Java Interop for Iris Data Analysis-Complete Program"><h2 id="Complete Program" class="header"><a href="#2024-02-03: Fantom&lt;-&gt;Java Interop for Iris Data Analysis-Complete Program">Complete Program</a></h2></div>

<p>
The complete program includes the <code>using</code> statements, to access the required
Java libraries and Fantom interop type.
</p>

<pre>
// IrisData analysis

using [java]org.apache.commons.math3.stat.descriptive::DescriptiveStatistics
using [java]org.apache.commons.math3.ml.clustering::CentroidCluster
using [java]org.apache.commons.math3.ml.clustering::Clusterable
using [java]org.apache.commons.math3.ml.clustering::KMeansPlusPlusClusterer
using [java]java.util::Arrays
using [java]fanx.interop::DoubleArray

class IrisData 
{
  static Void main(Str[] args) 
  {
    // step 1: read in CSV file
    echo("File name " + args[0])
    data := IrisInstance[,]

    util::CsvInStream(args[0].toUri.toFile.in).eachRow |Str[] row| 
    {
      if (row.size == 5)
      {
        data.add(IrisInstance(row))
      }
    }
    echo("Read " + data.size + " instances")

    // step 2: print out some statistics
    displayStatistics("Sepal Length", 
                      getStatistics(data) { it.sepalLength })
    displayStatistics("Sepal Width", 
                      getStatistics(data) { it.sepalWidth })
    displayStatistics("Petal Length", 
                      getStatistics(data) { it.petalLength })
    displayStatistics("Petal Width", 
                      getStatistics(data) { it.petalWidth })

    // step 3: cluster the instances
    model := KMeansPlusPlusClusterer(3)
    model.cluster(Arrays.asList(data)).toArray.each |CentroidCluster cluster|
    {
      echo("Centre: " + Arrays.toString(cluster.getCenter.getPoint))
      echo("Cluster has: " + cluster.getPoints.size + " points")
    }
  }

  static Void displayStatistics(Str name, DescriptiveStatistics statistics)
  {
    echo(name)
    echo(" -- Minimum: " + statistics.getMin.toLocale("0.00"))
    echo(" -- Maximum: " + statistics.getMax.toLocale("0.00"))
    echo(" -- Mean:    " + statistics.getMean.toLocale("0.00"))
    echo(" -- Stddev:  " + statistics.getStandardDeviation.toLocale("0.00"))
  }

  static DescriptiveStatistics getStatistics(IrisInstance[] data, 
                                             |IrisInstance->Float| accessor)
  {
    ds := DescriptiveStatistics()
    data.each { ds.addValue(accessor(it)) }
    return ds
  }
}

class IrisInstance : Clusterable
{
  Float sepalLength
  Float sepalWidth
  Float petalLength
  Float petalWidth
  Str label

  new make(Str[] row)
  {
    this.sepalLength = row[0].toFloat
    this.sepalWidth = row[1].toFloat
    this.petalLength = row[2].toFloat
    this.petalWidth = row[3].toFloat
    this.label = row[4]
  }

  override DoubleArray? getPoint() {
    result := DoubleArray(4)
    
    result[0] = sepalLength
    result[1] = sepalWidth
    result[2] = petalLength
    result[3] = petalWidth

    return result
  }
}
</pre>

    </div>
    <hr>
    <small>Page from Peter's <a href="../index.html">Scrapbook</a>, output from a <a href="http://vimwiki.github.io/">VimWiki</a> on 2024-02-06.</small>
</body>
</html>
