<!DOCTYPE html>
<html lang="en">
<head>
    <link rel="Stylesheet" type="text/css" href="../style.css" />
    <link rel="Stylesheet" type="text/css" href="../rouge-style.css" />
    <title>2024-10-24</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
</head>
<body>
    <div class="content">
    
<h1>2024-10-24: Visualising Local Git Contributions</h1>

<p>
I set out to rewrite in Lisp the program "Visualize your local Git
contributions with Go", which is described and listed at
<a href="https://flaviocopes.com/go-git-contributions/">https://flaviocopes.com/go-git-contributions/</a>
</p>

<p>
The aim is to generate a command-line program, which I called <code>git-commits</code>, 
with two basic operations. The first recursively adds all ".git" folders
under a given folder to a stored list:
</p>

 <img src="/images/2024-10-24-screenshot-1.png" />

<p>
The second produces commit statistics for a user identified by email
from the repositories in the stored list:
</p>

 <img src="/images/2024-10-24-screenshot-2.png" />

<p>
My final program is at: <a href="https://bitbucket.org/pclprojects/workspace/snippets/GzyRjK">https://bitbucket.org/pclprojects/workspace/snippets/GzyRjK</a>
</p>

<p>
The program uses some <a href="https://www.quicklisp.org/beta/">quicklisp</a> installable
libraries: 
</p>

<pre type=lisp>
<span class="p">(</span><span class="nb">load</span> <span class="s">"~/quicklisp/setup.lisp"</span><span class="p">)</span>           <span class="c1">; &lt;1&gt;</span>

<span class="p">(</span><span class="nv">ql:quickload</span> <span class="s">"cl-ansi-term"</span> <span class="ss">:silent</span> <span class="no">t</span><span class="p">)</span>   <span class="c1">; &lt;2&gt;</span>
<span class="p">(</span><span class="nv">ql:quickload</span> <span class="s">"cl-git"</span> <span class="ss">:silent</span> <span class="no">t</span><span class="p">)</span>
<span class="p">(</span><span class="nv">ql:quickload</span> <span class="s">"local-time"</span> <span class="ss">:silent</span> <span class="no">t</span><span class="p">)</span>
<span class="p">(</span><span class="nv">ql:quickload</span> <span class="s">"trivia"</span> <span class="ss">:silent</span> <span class="no">t</span><span class="p">)</span>
</pre>
<ol>
<li>
optionally load quicklisp if not using user settings

<li>
ignore warnings using <code>ql:quickload</code> for its <code>silent</code> feature. 

</ol>
 
<p>
Note, "cl-git" requires the libgit library, so:
</p>

<pre>
$ sudo apt install libgit2-dev
</pre>

<p>
The original tutorial does a good job of explaining the structure of the
program, and I followed it closely in my version. Following are some notes
about using the libraries.
</p>

<h2>Scanning Folders</h2>

<p>
The function below scans a given folder and its sub-folders, recursively, looking for 
all ".git" folders. It avoids going into "vendor" and "node_modules" folders - I put
this in because the original author had done so. All the work is done in the
<a href="https://asdf.common-lisp.dev/uiop.html">uiop</a> function <code>collect-sub*directories</code>:
</p>

<pre type=lisp>
<span class="p">(</span><span class="nb">defun</span> <span class="nv">recursive-scan-folder</span> <span class="p">(</span><span class="nv">folder</span><span class="p">)</span>
  <span class="s">"Collects .git folders, printing and returning their names."</span>
  <span class="p">(</span><span class="k">let</span> <span class="p">((</span><span class="nv">result</span> <span class="o">'</span><span class="p">()))</span>
    <span class="p">(</span><span class="nv">uiop:collect-sub*directories</span> 
      <span class="nv">folder</span>                <span class="c1">; &lt;1&gt;</span>
      <span class="p">(</span><span class="nb">constantly</span> <span class="no">t</span><span class="p">)</span>        <span class="c1">; &lt;2&gt;</span>
      <span class="p">(</span><span class="k">lambda</span> <span class="p">(</span><span class="nv">filepath</span><span class="p">)</span>    <span class="c1">; &lt;3&gt;</span>
        <span class="p">(</span><span class="nb">and</span> <span class="p">(</span><span class="nv">uiop:directory-pathname-p</span> <span class="nv">filepath</span><span class="p">)</span>
             <span class="p">(</span><span class="nb">not</span> <span class="p">(</span><span class="nb">equalp</span> <span class="s">"vendor"</span> <span class="p">(</span><span class="nb">first</span> <span class="p">(</span><span class="nb">last</span> <span class="p">(</span><span class="nb">pathname-directory</span> <span class="nv">filepath</span><span class="p">)))))</span>
             <span class="p">(</span><span class="nb">not</span> <span class="p">(</span><span class="nb">equalp</span> <span class="s">"node_modules"</span> <span class="p">(</span><span class="nb">first</span> <span class="p">(</span><span class="nb">last</span> <span class="p">(</span><span class="nb">pathname-directory</span> <span class="nv">filepath</span><span class="p">)))))))</span>
      <span class="p">(</span><span class="k">lambda</span> <span class="p">(</span><span class="nv">filepath</span><span class="p">)</span>    <span class="c1">; &lt;4&gt;</span>
        <span class="p">(</span><span class="nb">when</span> <span class="p">(</span><span class="nb">and</span> <span class="p">(</span><span class="nv">uiop:directory-pathname-p</span> <span class="nv">filepath</span><span class="p">)</span>
                   <span class="p">(</span><span class="nb">equalp</span> <span class="s">".git"</span> <span class="p">(</span><span class="nb">first</span> <span class="p">(</span><span class="nb">last</span> <span class="p">(</span><span class="nb">pathname-directory</span> <span class="nv">filepath</span><span class="p">)))))</span>
          <span class="p">(</span><span class="nb">write-line</span> <span class="p">(</span><span class="nb">namestring</span> <span class="nv">filepath</span><span class="p">))</span>
          <span class="p">(</span><span class="nb">push</span> <span class="nv">filepath</span> <span class="nv">result</span><span class="p">))))</span>
    <span class="p">(</span><span class="nb">reverse</span> <span class="nv">result</span><span class="p">)))</span>
</pre>
<ol>
<li>
The function takes four arguments, the first being the base folder to begin the search.

<li>
The second argument says whether we should collect within this directory - we return <code>T</code> to search everywhere.

<li>
The third argument says whether we should recurse within this directory - we reject "vendor" and "node_modules".

<li>
The fourth argument checks if it has a ".git" directory, then writes out its name and collects it in <code>result</code> list.

</ol>
<h2>Analysing a Git Repository</h2>

<p>
Analysing the Git repository is fairly straightforward, thanks to the 
<a href="https://cl-git.russellsim.org/">cl-git</a> library.
</p>

<pre type=lisp>
<span class="p">(</span><span class="nb">defun</span> <span class="nv">fill-commits</span> <span class="p">(</span><span class="nv">commits</span> <span class="nv">email</span> <span class="nv">filepath</span><span class="p">)</span>
  <span class="s">"Add information from repository in filepath to commits hash-table."</span>
  <span class="p">(</span><span class="nv">cl-git:with-repository</span>                                               <span class="c1">; &lt;1&gt;</span>
    <span class="p">(</span><span class="nv">repository</span> <span class="nv">filepath</span><span class="p">)</span>
    <span class="p">(</span><span class="k">let*</span> <span class="p">((</span><span class="nv">ref</span> <span class="p">(</span><span class="nv">cl-git:repository-head</span> <span class="nv">repository</span><span class="p">)))</span>                   <span class="c1">; &lt;2&gt;</span>
      <span class="p">(</span><span class="nb">dolist</span> <span class="p">(</span><span class="nv">reflog</span> <span class="p">(</span><span class="nv">cl-git:entries</span> <span class="p">(</span><span class="nv">cl-git:reflog</span> <span class="nv">ref</span><span class="p">)))</span>             <span class="c1">; &lt;3&gt;</span>
        <span class="p">(</span><span class="k">let</span> <span class="p">((</span><span class="nv">commit-email</span> <span class="p">(</span><span class="nb">getf</span> <span class="p">(</span><span class="nv">cl-git:committer</span> <span class="nv">reflog</span><span class="p">)</span> <span class="ss">:email</span><span class="p">))</span>    <span class="c1">; &lt;4&gt;</span>
              <span class="p">(</span><span class="nv">commit-datetime</span> <span class="p">(</span><span class="nb">getf</span> <span class="p">(</span><span class="nv">cl-git:committer</span> <span class="nv">reflog</span><span class="p">)</span> <span class="ss">:time</span><span class="p">)))</span>
          <span class="p">(</span><span class="nb">when</span> <span class="p">(</span><span class="nb">equalp</span> <span class="nv">email</span> <span class="nv">commit-email</span><span class="p">)</span> <span class="c1">; requested email did the commit</span>
            <span class="p">(</span><span class="k">let</span> <span class="p">((</span><span class="nv">days-ago</span> <span class="p">(</span><span class="nv">count-days-since-date</span> <span class="nv">commit-datetime</span><span class="p">)))</span>
              <span class="p">(</span><span class="nb">when</span> <span class="nv">days-ago</span> <span class="c1">; only proceed if within last six months</span>
                <span class="p">(</span><span class="nb">incf</span> <span class="p">(</span><span class="nb">gethash</span> <span class="p">(</span><span class="nb">+</span> <span class="nv">days-ago</span> <span class="p">(</span><span class="nv">calc-offset</span><span class="p">))</span> <span class="nv">commits</span> <span class="mi">0</span><span class="p">))))))))))</span>
</pre>
<ol>
<li>
Opens a repository in given filepath

<li>
Finds HEAD of repository

<li>
Retrieves a list of commit instances from the HEAD's references.

<li>
Information about the committer is held as a property list, so use <code>getf</code> to
   retrieve <code>:email</code> and <code>:time</code>.

</ol>
<h2>Date/Time Processing</h2>

<p>
This program has a lot of date/time processing steps: we have to check if
commits are within the last six months, which day of the week they are on,
which month, etc. Like "cl-git", we rely on
<a href="https://local-time.common-lisp.dev/manual.html">local-time</a>.  A good example
is <code>print-months</code>:
</p>

<pre type=lisp>
<span class="p">(</span><span class="nb">defun</span> <span class="nv">print-months</span> <span class="p">()</span>
  <span class="s">"Prints month names in first lines, determining month change in switching weeks."</span>
  <span class="p">(</span><span class="nb">write-string</span> <span class="s">"         "</span><span class="p">)</span>
  <span class="p">(</span><span class="nb">do*</span> <span class="p">((</span><span class="nv">week</span> <span class="p">(</span><span class="nv">local-time:timestamp-</span> <span class="p">(</span><span class="nv">local-time:today</span><span class="p">)</span> <span class="nv">+DAYS-IN-LAST-SIX-MONTHS+</span> <span class="ss">:day</span><span class="p">)</span>   <span class="c1">; &lt;1&gt;</span>
              <span class="p">(</span><span class="nv">local-time:timestamp+</span> <span class="nv">week</span> <span class="mi">7</span> <span class="ss">:day</span><span class="p">))</span>                                        <span class="c1">; &lt;2&gt;</span>
        <span class="p">(</span><span class="nv">month</span> <span class="p">(</span><span class="nv">local-time:timestamp-month</span> <span class="nv">week</span><span class="p">)))</span>                                        <span class="c1">; &lt;3&gt;</span>
    <span class="p">((</span><span class="nv">local-time:timestamp&gt;</span> <span class="nv">week</span> <span class="p">(</span><span class="nv">local-time:today</span><span class="p">))</span>                                      <span class="c1">; &lt;4&gt;</span>
     <span class="p">(</span><span class="nb">write-line</span> <span class="s">""</span><span class="p">))</span>
    <span class="c1">;</span>
    <span class="p">(</span><span class="k">if</span> <span class="p">(</span><span class="nb">=</span> <span class="p">(</span><span class="nv">local-time:timestamp-month</span> <span class="nv">week</span><span class="p">)</span> <span class="nv">month</span><span class="p">)</span>                                       <span class="c1">; &lt;5&gt;</span>
      <span class="p">(</span><span class="nb">write-string</span> <span class="s">"    "</span><span class="p">)</span>
      <span class="p">(</span><span class="k">progn</span> <span class="c1">; print-month string</span>
        <span class="p">(</span><span class="nb">format</span> <span class="no">t</span> <span class="s">"~a "</span> <span class="p">(</span><span class="nv">local-time:format-timestring</span> <span class="no">nil</span> <span class="nv">week</span> <span class="ss">:format</span> <span class="o">'</span><span class="p">(</span><span class="ss">:short-month</span><span class="p">)))</span>  <span class="c1">; &lt;6&gt;</span>
        <span class="p">(</span><span class="nb">setf</span> <span class="nv">month</span> <span class="p">(</span><span class="nv">local-time:timestamp-month</span> <span class="nv">week</span><span class="p">))))))</span>
</pre>
<ol>
<li>
<code>week</code> is set to the day six-months ago

<li>
and <code>week</code> is advanced 7 days (1 week) at a time

<li>
<code>month</code> is first set to the month of the initial <code>week</code>

<li>
printing stopes when the <code>week</code> is greater than "today".

<li>
checks if the current week's month is the same as the stored <code>month</code> - if it
   is, we don't print a new name

<li>
otherwise, we can use a time format string to extract the month name

</ol>
<h2>Coloured Terminal Output</h2>

<p>
Getting some coloured terminal output required some bigger changes to the
original program.  I decided to use another library,
<a href="https://vindarel.github.io/cl-ansi-term/">cl-ansi-term</a>, which supports
formatting of text output.  One structural difference is that the library
requires output for a whole line to be collected into a list, and then printed 
in one step.
</p>

<p>
I first adjusted the terminal width and created some styles:
</p>

<pre type=lisp>
<span class="p">(</span><span class="nb">setf</span> <span class="nv">term:*terminal-width*</span> <span class="mi">120</span><span class="p">)</span>
<span class="p">(</span><span class="nv">term:update-style-sheet</span> <span class="o">'</span><span class="p">((</span><span class="ss">:small</span> <span class="ss">:black</span> <span class="ss">:b-white</span><span class="p">)</span>
                           <span class="p">(</span><span class="ss">:medium</span> <span class="ss">:black</span> <span class="ss">:b-yellow</span><span class="p">)</span>
                           <span class="p">(</span><span class="ss">:high</span> <span class="ss">:black</span> <span class="ss">:b-green</span><span class="p">)</span>
                           <span class="p">(</span><span class="ss">:today</span> <span class="ss">:white</span> <span class="ss">:b-magenta</span><span class="p">)</span>
                           <span class="p">(</span><span class="ss">:empty</span> <span class="ss">:default</span> <span class="ss">:b-default</span><span class="p">)))</span>
</pre>

<p>
The idea is that <code>:small</code> will be a "black text on white background" style for
commit counts from 1 to 4, etc: this library makes it very easy to set up
foreground/background colours for text.
</p>

<p>
To manage the collection of output strings and style, the <code>print-cell</code> function
is changed to <code>get-cell</code> which returns a list with the first value being the
string to display and the second value the name of the style to use. For
example: <code>(get-cell 2 nil)</code> returns <code>("  2 " :small)</code>.
</p>

<pre type=lisp>
<span class="p">(</span><span class="nb">defun</span> <span class="nv">get-cell</span> <span class="p">(</span><span class="nv">value</span> <span class="nv">today</span><span class="p">)</span>
  <span class="s">"Given a cell value, groups with style depending on value and today flag."</span>
  <span class="p">(</span><span class="nb">list</span> <span class="p">(</span><span class="k">if</span> <span class="p">(</span><span class="nb">zerop</span> <span class="nv">value</span><span class="p">)</span> <span class="s">"  - "</span> <span class="p">(</span><span class="nb">format</span> <span class="no">nil</span> <span class="s">"~3d "</span> <span class="nv">value</span><span class="p">))</span>
        <span class="p">(</span><span class="nb">cond</span> <span class="p">((</span><span class="nb">and</span> <span class="p">(</span><span class="nb">&lt;</span> <span class="mi">0</span> <span class="nv">value</span><span class="p">)</span> <span class="p">(</span><span class="nb">&lt;</span> <span class="nv">value</span> <span class="mi">5</span><span class="p">))</span>
               <span class="ss">:small</span><span class="p">)</span>
              <span class="p">((</span><span class="nb">and</span> <span class="p">(</span><span class="nb">&lt;=</span> <span class="mi">5</span> <span class="nv">value</span><span class="p">)</span> <span class="p">(</span><span class="nb">&lt;</span> <span class="nv">value</span> <span class="mi">10</span><span class="p">))</span>
               <span class="ss">:medium</span><span class="p">)</span>
              <span class="p">((</span><span class="nb">&lt;=</span> <span class="mi">10</span> <span class="nv">value</span><span class="p">)</span>
               <span class="ss">:high</span><span class="p">)</span>
              <span class="p">(</span><span class="nv">today</span>
                <span class="ss">:today</span><span class="p">)</span>
              <span class="p">(</span><span class="no">t</span>
                <span class="ss">:empty</span><span class="p">))))</span>
</pre>

<p>
The <code>print-cells</code> function is adjusted to collect these pairs for each line,
and output them in one go at the end of the line:
</p>

<pre type=lisp>
<span class="p">(</span><span class="nb">defun</span> <span class="nv">print-cells</span> <span class="p">(</span><span class="nv">columns</span><span class="p">)</span>
  <span class="p">(</span><span class="nv">print-months</span><span class="p">)</span>
  <span class="p">(</span><span class="nb">do</span> <span class="p">((</span><span class="nv">j</span> <span class="mi">6</span> <span class="p">(</span><span class="nb">-</span> <span class="nv">j</span> <span class="mi">1</span><span class="p">)))</span>
    <span class="p">((</span><span class="nb">minusp</span> <span class="nv">j</span><span class="p">)</span> <span class="p">)</span>
    <span class="p">(</span><span class="k">let</span> <span class="p">((</span><span class="nv">line-defn</span> <span class="o">'</span><span class="p">()))</span>                                    <span class="c1">; &lt;1&gt;</span>
      <span class="p">(</span><span class="nb">do</span> <span class="p">((</span><span class="nv">i</span> <span class="p">(</span><span class="nb">+</span> <span class="nv">+WEEKS-IN-LAST-SIX-MONTHS+</span> <span class="mi">1</span><span class="p">)</span> <span class="p">(</span><span class="nb">-</span> <span class="nv">i</span> <span class="mi">1</span><span class="p">)))</span>
        <span class="p">((</span><span class="nb">minusp</span> <span class="nv">i</span><span class="p">)</span> <span class="p">(</span><span class="nv">term:cat-print</span> <span class="p">(</span><span class="nb">reverse</span> <span class="nv">line-defn</span><span class="p">)))</span>     <span class="c1">; &lt;2&gt;</span>
        <span class="p">(</span><span class="nb">when</span> <span class="p">(</span><span class="nb">=</span> <span class="nv">i</span> <span class="p">(</span><span class="nb">+</span> <span class="nv">+WEEKS-IN-LAST-SIX-MONTHS+</span> <span class="mi">1</span><span class="p">))</span>
          <span class="p">(</span><span class="nb">push</span> <span class="p">(</span><span class="nv">get-day-col</span> <span class="nv">j</span><span class="p">)</span> <span class="nv">line-defn</span><span class="p">))</span>                   <span class="c1">; &lt;3&gt;</span>
        <span class="p">(</span><span class="k">let</span> <span class="p">((</span><span class="nv">col</span> <span class="p">(</span><span class="nb">gethash</span> <span class="nv">i</span> <span class="nv">columns</span><span class="p">)))</span>
          <span class="p">(</span><span class="nb">cond</span> <span class="p">((</span><span class="nb">and</span> <span class="nv">col</span>
                      <span class="p">(</span><span class="nb">zerop</span> <span class="nv">i</span><span class="p">)</span>
                      <span class="p">(</span><span class="nb">=</span> <span class="nv">j</span> <span class="p">(</span><span class="nb">-</span> <span class="p">(</span><span class="nv">calc-offset</span><span class="p">)</span> <span class="mi">1</span><span class="p">)))</span>
                 <span class="p">(</span><span class="nb">push</span> <span class="p">(</span><span class="nv">get-cell</span> <span class="p">(</span><span class="nb">elt</span> <span class="nv">col</span> <span class="nv">j</span><span class="p">)</span> <span class="no">t</span><span class="p">)</span> <span class="nv">line-defn</span><span class="p">))</span>   <span class="c1">; &lt;4&gt;</span>
                <span class="p">((</span><span class="nb">and</span> <span class="nv">col</span>
                      <span class="p">(</span><span class="nb">&gt;</span> <span class="p">(</span><span class="nb">length</span> <span class="nv">col</span><span class="p">)</span> <span class="nv">j</span><span class="p">))</span>
                 <span class="p">(</span><span class="nb">push</span> <span class="p">(</span><span class="nv">get-cell</span> <span class="p">(</span><span class="nb">elt</span> <span class="nv">col</span> <span class="nv">j</span><span class="p">)</span> <span class="no">nil</span><span class="p">)</span> <span class="nv">line-defn</span><span class="p">))</span> <span class="c1">; &lt;4&gt;</span>
                <span class="p">(</span><span class="no">t</span>
                  <span class="p">(</span><span class="nb">push</span> <span class="p">(</span><span class="nv">get-cell</span> <span class="mi">0</span> <span class="no">nil</span><span class="p">)</span> <span class="nv">line-defn</span><span class="p">))))))))</span>    <span class="c1">; &lt;4&gt;</span>
</pre>
<ol>
<li>
<code>line-defn</code> is a list to collect all the string-style pairs for printing

<li>
at the end of a line, <code>cat-print</code> is used to print the string-style pairs

<li>
collects the day-of-week name for the first column

<li>
these calls get the string-style pair for each cell in the row and add 
   them to <code>line-defn</code> 

</ol>
<h2>Top Level</h2>

<p>
For the top level, because we only have two simple commands, a simple
pattern-match is enough to call the relevant function, add or email.  The
handler catches errors, and displays a simple message in each case.
</p>

<pre type=lisp>
<span class="p">(</span><span class="nb">defun</span> <span class="nv">main</span> <span class="p">()</span>
  <span class="s">"CLI start point"</span>
  <span class="p">(</span><span class="nb">handler-case</span>                                 <span class="c1">; &lt;1&gt;</span>
    <span class="p">(</span><span class="nv">trivia:match</span>
      <span class="p">(</span><span class="nv">uiop:command-line-arguments</span><span class="p">)</span>             <span class="c1">; &lt;2&gt;</span>
      <span class="p">((</span><span class="nb">list</span> <span class="s">"add"</span> <span class="nv">folder</span><span class="p">)</span> <span class="p">(</span><span class="nv">scan</span> <span class="nv">folder</span><span class="p">))</span>       <span class="c1">; &lt;3&gt;</span>
      <span class="p">((</span><span class="nb">list</span> <span class="s">"email"</span> <span class="nv">address</span><span class="p">)</span> <span class="p">(</span><span class="nv">stats</span> <span class="nv">address</span><span class="p">))</span>
      <span class="p">(</span><span class="nv">otherwise</span>                                <span class="c1">; &lt;4&gt;</span>
        <span class="p">(</span><span class="nb">write-line</span> <span class="s">"gits-commits: Visualise your git commits"</span><span class="p">)</span>
        <span class="p">(</span><span class="nb">write-line</span> <span class="s">"gits-commits add FOLDER - adds all .git repos under FOLDER for scanning"</span><span class="p">)</span>
        <span class="p">(</span><span class="nb">write-line</span> <span class="s">"gits-commits email EMAIL - generates statistics for added repos by given email"</span><span class="p">)))</span>
    <span class="p">(</span><span class="nv">cl-git:not-found-error</span> <span class="p">(</span><span class="nv">e</span><span class="p">)</span>                 <span class="c1">; &lt;5&gt; </span>
                            <span class="p">(</span><span class="nb">format</span> <span class="no">t</span> <span class="s">"~a~&amp;"</span> <span class="nv">e</span><span class="p">)</span>
                            <span class="p">(</span><span class="nb">format</span> <span class="no">t</span> <span class="s">"Suggest deleting dot file and re-adding repositories.~&amp;"</span><span class="p">))</span>
    <span class="p">(</span><span class="no">t</span> <span class="p">()</span>                                       <span class="c1">; &lt;6&gt;</span>
       <span class="p">(</span><span class="nb">write-line</span> <span class="s">"Error: there was an error in running the program."</span><span class="p">))))</span>
</pre>
<ol>
<li>
Prepare to catch any errors

<li>
match on the list of command-line arguments

<li>
case one is the "add" command

<li>
case three shows some help information

<li>
Handle a typical git error

<li>
Handle all other errors

</ol>
<h2>Binary Executable</h2>

<p>
To create a binary, you <em>should</em> be able to simply write:
</p>

<pre>
$ sbcl
&gt; (load "git-commits.lisp")
&gt; (sb-ext:save-lisp-and-die "git-commits" :executable t :save-runtime-options t :toplevel 'gitcommits:main)
</pre>

<p>
but I get an error when I try to run the binary:
</p>

<pre>
$ ./git-commits email peterlane@gmx.com

debugger invoked on a CL-GIT:GENERAL-ERROR in thread
#&lt;THREAD tid=11724 "main thread" RUNNING {1001F78003}&gt;:
  Error :INVALID, libgit2 has not been initialized; you must call git_libgit2_init

Type HELP for debugger help, or (SB-EXT:EXIT) to exit from SBCL.

restarts (invokable by number or by possibly-abbreviated name):
  0: [ABORT] Exit from the current thread.

((:METHOD CFFI:TRANSLATE-FROM-FOREIGN (T CL-GIT::RETURN-VALUE-TYPE)) -1 #&lt;unused argument&gt;) [fast-method]
   source: (ERROR (GETHASH RETURN-VALUE ERROR-CONDITIONS 'UNKNOWN-ERROR) :CODE
                  RETURN-VALUE :MESSAGE (CADR LAST-ERROR) :CLASS
                  (CAR LAST-ERROR))
0] 0
</pre>

<p>
So, putting a call to <code>(main)</code> at the bottom of the file and using <code>sbcl --load gits-commits.lisp</code> is the way to go ...
</p>

    </div>
    <hr>
    <small>Page from Peter's <a href="../index.html">Scrapbook</a>, output from a <a href="http://vimwiki.github.io/">VimWiki</a> on 2024-11-29.</small>
</body>
</html>
