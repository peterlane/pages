<!DOCTYPE html>
<html lang="en">
<head>
    <link rel="Stylesheet" type="text/css" href="../style.css" />
    <link rel="Stylesheet" type="text/css" href="../rouge-style.css" />
    <title>2024-10-28</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
</head>
<body>
    <div class="content">
    
<h1>2024-10-28: String Splitting</h1>

<p>
I've been looking at one of the first examples used on the Raku language site:
<a href="https://docs.raku.org/language/101-basics">Raku by Example</a>.  This provides a
little test case for exploring some string splitting and related Lisp
libraries.
</p>

<p>
The program reads in a file of data and does some pre-processing.
What struck me as interesting was the way the data was split up 
into fields. 
</p>

<p>
The file format for a single line is:
</p>

<pre>
Ana Dave | 3:0
</pre>

<p>
and the Raku code to get this into four variables is:
</p>

<pre>
    my ($pairing, $result) = $line.split(' | ');
    my ($p1, $p2)          = $pairing.words;
    my ($r1, $r2)          = $result.split(':');
</pre>

<p>
What does this look like in Lisp?
</p>

<h2>No Libraries</h2>

<p>
The "no library" version could be written as:
</p>

<pre type=lisp>
       <span class="p">(</span><span class="k">let*</span> <span class="p">((</span><span class="nv">space-posn</span> <span class="p">(</span><span class="nb">position</span> <span class="sc">#\space</span> <span class="nv">line</span><span class="p">))</span>
              <span class="p">(</span><span class="nv">bar-posn</span> <span class="p">(</span><span class="nb">position</span> <span class="sc">#\|</span> <span class="nv">line</span><span class="p">))</span>
              <span class="p">(</span><span class="nv">colon-posn</span> <span class="p">(</span><span class="nb">position</span> <span class="sc">#\:</span> <span class="nv">line</span><span class="p">))</span>
              <span class="p">(</span><span class="nv">pairing-1</span> <span class="p">(</span><span class="nb">subseq</span> <span class="nv">line</span> <span class="mi">0</span> <span class="nv">space-posn</span><span class="p">))</span>
              <span class="p">(</span><span class="nv">pairing-2</span> <span class="p">(</span><span class="nb">subseq</span> <span class="nv">line</span> <span class="p">(</span><span class="nb">+</span> <span class="nv">space-posn</span> <span class="mi">1</span><span class="p">)</span> <span class="p">(</span><span class="nb">-</span> <span class="nv">bar-posn</span> <span class="mi">1</span><span class="p">)))</span>
              <span class="p">(</span><span class="nv">result-1</span> <span class="p">(</span><span class="nb">subseq</span> <span class="nv">line</span> <span class="p">(</span><span class="nb">+</span> <span class="nv">bar-posn</span> <span class="mi">2</span><span class="p">)</span> <span class="nv">colon-posn</span><span class="p">))</span>
              <span class="p">(</span><span class="nv">result-2</span> <span class="p">(</span><span class="nb">subseq</span> <span class="nv">line</span> <span class="p">(</span><span class="nb">+</span> <span class="nv">colon-posn</span> <span class="mi">1</span><span class="p">))))</span>
       <span class="c1">; ...</span>
       <span class="p">)</span>
</pre>

<p>
This uses <code>position</code> to extract the index of the three key split points in 
the string, and then careful <code>subseq</code> operations to retrieve the four 
required values.
</p>

<p>
This is not too bad to write for small examples, but is clearly not robust and
it's not too clear how the splitting relates to the structure of the string.
</p>

<h2>Using UIOP Library</h2>

<p>
The <a href="https://asdf.common-lisp.dev/uiop.html">UIOP</a> library is an important
one, because it comes along with ASDF, so you get it "for free" when using the
library system. The library offers a useful function, <code>split-string</code>, which 
splits strings based on one or more separating characters. 
</p>

<p>
In this way, our string is split by using all three separator characters in one
go:
</p>

<pre>
* (uiop:split-string "Ann Dave | 3:0" :separator " |:")
("Ann" "Dave" "" "" "3" "0")
</pre>

<p>
The above code can then be rewritten, using <code>destructuring-bind</code> and removing all
the empty strings:
</p>

<pre type=lisp>
        <span class="p">(</span><span class="nb">destructuring-bind</span> <span class="p">(</span><span class="nv">pairing-1</span> <span class="nv">pairing-2</span> <span class="nv">result-1</span> <span class="nv">result-2</span><span class="p">)</span>
          <span class="p">(</span><span class="nb">remove-if</span> <span class="nf">#'</span><span class="nv">uiop:emptyp</span> <span class="p">(</span><span class="nv">uiop:split-string</span> <span class="nv">line</span> <span class="ss">:separator</span> <span class="s">" |:"</span><span class="p">))</span>
        <span class="c1">; ...</span>
        <span class="p">)</span>
</pre>

<p>
This works, but it again ignores the structure of the data, which the Raku 
program reflected: first splitting the data into two halves, and then dealing
with each half in a different way. 
</p>

<p>
The Raku <code>split</code> command also has an additional ability over uiop's
<code>split-string</code>: it can split a string using a substring, returning the parts
before and after the substring.
</p>

<pre>
[3] &gt; 'a-b-c | d-e-f'.split(' | ')
(a-b-c d-e-f)
[4] &gt; '(a-b-c | d-e-f'.split('|')
((a-b-c   d-e-f)
</pre>

<p>
The first split has removed the vertical bar and surrounding spaces. Can we 
do this in Lisp?
</p>

<h2>Using str Library</h2>

<p>
The <a href="https://vindarel.github.io/cl-str/#/">str</a> library provides many important 
string-processing functions. 
</p>

<p>
The function we are looking at is str's <code>split</code> function. This does several
things, but, to answer the points above, it can split on a character or on a
substring:
</p>

<pre>
* (str:split #\| "Ann Dave | 3:0")
("Ann Dave " " 3:0")
* (str:split " | " "Ann Dave | 3:0")
("Ann Dave" "3:0")
</pre>

<p>
and it has a flag to remove empty strings from the result:
</p>

<pre>
* (str:split #\space " one two  three ")
("" "one" "two" "" "three" "")
* (str:split #\space " one two  three " :omit-nulls t)
("one" "two" "three")
</pre>

<p>
We use <code>split</code> and <code>words</code> to replicate the three split statements from the
Raku code:
</p>

<pre type=lisp>
        <span class="p">(</span><span class="nb">destructuring-bind</span> <span class="p">(</span><span class="nv">pairing</span> <span class="nv">result</span><span class="p">)</span> <span class="p">(</span><span class="nv">str:split</span> <span class="s">" | "</span> <span class="nv">line</span><span class="p">)</span>
          <span class="p">(</span><span class="nb">destructuring-bind</span> <span class="p">(</span><span class="nv">pairing-1</span> <span class="nv">pairing-2</span><span class="p">)</span> <span class="p">(</span><span class="nv">str:words</span> <span class="nv">pairing</span><span class="p">)</span>
            <span class="p">(</span><span class="nb">destructuring-bind</span> <span class="p">(</span><span class="nv">result-1</span> <span class="nv">result-2</span><span class="p">)</span> <span class="p">(</span><span class="nv">str:split</span> <span class="s">":"</span> <span class="nv">result</span><span class="p">)</span>
        <span class="c1">; ...</span>
        <span class="p">)))</span>
</pre>

<p>
Of course, the binding part looks a bit ugly with those nested, repeated
<code>destructuring-bind</code> statements. We clean this up with another library: the
<a href="https://metabang-bind.common-lisp.dev/user-guide.html">metabang-bind</a>
library, which provides a combination of <code>let</code>, <code>destructuring-bind</code>,
<code>with-multiple-values</code>.
</p>

<p>
With this, we write three parallel assignments, splitting the line in steps, 
echoing the original Raku program:
</p>

<pre type=lisp>
        <span class="p">(</span><span class="nv">metabang-bind:bind</span> 
          <span class="p">(((</span><span class="nv">pairing</span> <span class="nv">result</span><span class="p">)</span> <span class="p">(</span><span class="nv">str:split</span> <span class="s">" | "</span> <span class="nv">line</span><span class="p">))</span>
           <span class="p">((</span><span class="nv">pairing-1</span> <span class="nv">pairing-2</span><span class="p">)</span> <span class="p">(</span><span class="nv">str:words</span> <span class="nv">pairing</span><span class="p">))</span>
           <span class="p">((</span><span class="nv">result-1</span> <span class="nv">result-2</span><span class="p">)</span> <span class="p">(</span><span class="nv">str:split</span> <span class="s">":"</span> <span class="nv">result</span><span class="p">)))</span>
        <span class="c1">; ...</span>
        <span class="p">)</span>
</pre>

<p>
The only remaining point about Raku's treatment of the input data, is that
Raku is weakly typed, and is happy to take the strings returned for the results
and use them as numbers:
</p>

<pre>
[0] &gt; 1 + 2
3
[1] &gt; 1 + '2'
3
[2] &gt; "1" + 2
3
[3] &gt; "1" + "2"
3
</pre>

<p>
whereas strongly-typed Lisp is not so happy:
</p>

<pre>
* (+ 1 2)
3
* (+ 1 "2")

debugger invoked on a TYPE-ERROR @B800002FFF in thread
#&lt;THREAD tid=2954 "main thread" RUNNING {1000B90003}&gt;:
  The value
    "2"
  is not of type
    NUMBER
</pre>

<p>
For Lisp, we need to convert the results to integers, which is done after
splitting the <code>result</code> variable.
</p>

<h2>Final Program</h2>

<p>
For completeness, my final Lisp program is:
</p>

<pre type=lisp>
<span class="p">(</span><span class="nv">asdf:load-system</span> <span class="s">"metabang-bind"</span><span class="p">)</span>
<span class="p">(</span><span class="nv">asdf:load-system</span> <span class="s">"str"</span><span class="p">)</span>

<span class="p">(</span><span class="nb">format</span> <span class="no">t</span> <span class="s">"Tournament results:~%~%"</span><span class="p">)</span>

<span class="p">(</span><span class="k">let*</span> <span class="p">((</span><span class="nv">lines</span> <span class="p">(</span><span class="nv">uiop:read-file-lines</span> <span class="s">"scores.txt"</span><span class="p">))</span>
       <span class="p">(</span><span class="nv">names</span> <span class="p">(</span><span class="nv">str:words</span> <span class="p">(</span><span class="nb">first</span> <span class="nv">lines</span><span class="p">)))</span>
       <span class="p">(</span><span class="nv">matches</span> <span class="p">(</span><span class="nb">make-hash-table</span> <span class="ss">:test</span> <span class="nf">#'</span><span class="nb">equalp</span><span class="p">))</span>
       <span class="p">(</span><span class="nv">sets</span> <span class="p">(</span><span class="nb">make-hash-table</span> <span class="ss">:test</span> <span class="nf">#'</span><span class="nb">equalp</span><span class="p">)))</span>
  <span class="c1">;</span>
  <span class="p">(</span><span class="nb">dolist</span> <span class="p">(</span><span class="nv">line</span> <span class="p">(</span><span class="nb">rest</span> <span class="nv">lines</span><span class="p">))</span>
    <span class="p">(</span><span class="nb">unless</span> <span class="p">(</span><span class="nv">str:emptyp</span> <span class="nv">line</span><span class="p">)</span>
      <span class="p">(</span><span class="nv">metabang-bind:bind</span> 
        <span class="p">(((</span><span class="nv">pairing</span> <span class="nv">result</span><span class="p">)</span> <span class="p">(</span><span class="nv">str:split</span> <span class="s">" | "</span> <span class="nv">line</span><span class="p">))</span>
         <span class="p">((</span><span class="nv">player-1</span> <span class="nv">player-2</span><span class="p">)</span> <span class="p">(</span><span class="nv">str:words</span> <span class="nv">pairing</span><span class="p">))</span>
         <span class="p">((</span><span class="nv">result-1</span> <span class="nv">result-2</span><span class="p">)</span> <span class="p">(</span><span class="nb">mapcar</span> <span class="nf">#'</span><span class="nb">parse-integer</span>
                                      <span class="p">(</span><span class="nv">str:split</span> <span class="s">":"</span> <span class="nv">result</span><span class="p">))))</span>

        <span class="p">(</span><span class="nb">incf</span> <span class="p">(</span><span class="nb">gethash</span> <span class="nv">player-1</span> <span class="nv">sets</span> <span class="mi">0</span><span class="p">)</span> <span class="nv">result-1</span><span class="p">)</span>
        <span class="p">(</span><span class="nb">incf</span> <span class="p">(</span><span class="nb">gethash</span> <span class="nv">player-2</span> <span class="nv">sets</span> <span class="mi">0</span><span class="p">)</span> <span class="nv">result-2</span><span class="p">)</span>
        <span class="p">(</span><span class="k">if</span> <span class="p">(</span><span class="nb">&gt;</span> <span class="nv">result-1</span> <span class="nv">result-2</span><span class="p">)</span>
          <span class="p">(</span><span class="nb">incf</span> <span class="p">(</span><span class="nb">gethash</span> <span class="nv">player-1</span> <span class="nv">matches</span> <span class="mi">0</span><span class="p">))</span>
          <span class="p">(</span><span class="nb">incf</span> <span class="p">(</span><span class="nb">gethash</span> <span class="nv">player-2</span> <span class="nv">matches</span> <span class="mi">0</span><span class="p">))))))</span>
  <span class="c1">;</span>
  <span class="p">(</span><span class="nb">setf</span> <span class="nv">names</span> <span class="p">(</span><span class="nb">sort</span> <span class="nv">names</span> <span class="nf">#'</span><span class="nb">&lt;</span> <span class="ss">:key</span> <span class="nf">#'</span><span class="p">(</span><span class="k">lambda</span> <span class="p">(</span><span class="nv">name</span><span class="p">)</span> <span class="p">(</span><span class="nb">gethash</span> <span class="nv">name</span> <span class="nv">sets</span><span class="p">))))</span>
  <span class="p">(</span><span class="nb">setf</span> <span class="nv">names</span> <span class="p">(</span><span class="nb">sort</span> <span class="nv">names</span> <span class="nf">#'</span><span class="nb">&lt;</span> <span class="ss">:key</span> <span class="nf">#'</span><span class="p">(</span><span class="k">lambda</span> <span class="p">(</span><span class="nv">name</span><span class="p">)</span> <span class="p">(</span><span class="nb">gethash</span> <span class="nv">name</span> <span class="nv">matches</span><span class="p">))))</span>
  <span class="c1">;</span>
  <span class="p">(</span><span class="nb">dolist</span> <span class="p">(</span><span class="nv">name</span> <span class="p">(</span><span class="nb">reverse</span> <span class="nv">names</span><span class="p">))</span>
    <span class="p">(</span><span class="nb">format</span> <span class="no">t</span> <span class="s">"~a has won ~d ~:* ~[matches~;match~:;matches~] and ~d set~:p~&amp;"</span>
            <span class="nv">name</span> 
            <span class="p">(</span><span class="nb">gethash</span> <span class="nv">name</span> <span class="nv">matches</span><span class="p">)</span>
            <span class="p">(</span><span class="nb">gethash</span> <span class="nv">name</span> <span class="nv">sets</span><span class="p">))))</span>

<span class="p">(</span><span class="nv">uiop:quit</span><span class="p">)</span>
</pre>

<p>
Note the use of <code>format</code> conditionals to handle plurals.
</p>

<ul>
<li>
<code>~:*</code> steps back in the list of arguments, to reuse the count of matches

<li>
<code>~[...~]</code> picks the relevant match/matches based on if the count is 0, 1, or more

<li>
<code>~:p</code> adds an "s" if the previous argument is not 1

</ul>

    </div>
    <hr>
    <small>Page from Peter's <a href="../index.html">Scrapbook</a>, output from a <a href="http://vimwiki.github.io/">VimWiki</a> on 2024-11-29.</small>
</body>
</html>
